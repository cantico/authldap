<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';


// We use oxygen if it is available.
if ($icons = @bab_functionality::get('Icons/Oxygen')) {
    $icons->includeCss();
} else if ($icons = @bab_functionality::get('Icons')) {
    $icons->includeCss();
}

/**
 * 
 * @param string $str
 * @return string
 */
function authldap_translate($str, $str_plurals = null, $number = null)
{
    if ($translate = bab_functionality::get('Translate/Gettext'))
    {
        /* @var $translate Func_Translate_Gettext */
        $translate->setAddonName('authldap');

        return $translate->translate($str, $str_plurals, $number);
    }

    return $str;
}




/**
 * 
 * @return bab_Registry
 */
function authldap_registry()
{
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/authldap/');
	
	return $registry;
}


/**
 * @return authldap_Controller
 */
function authldap_Controller()
{
    require_once dirname(__FILE__) . '/controller.class.php';
    return bab_getInstance('authldap_Controller');
}

function authldap_IncludeSet()
{
    require_once dirname(__FILE__).'/set/server.class.php';
    require_once dirname(__FILE__).'/set/siteLink.class.php';
    require_once dirname(__FILE__).'/set/user.class.php';
    require_once dirname(__FILE__).'/set/siteOption.class.php';
    require_once dirname(__FILE__).'/set/notifiedGroups.class.php';
}

function authldap_ServerSet()
{
    authldap_IncludeSet();
    return new authldap_ServerSet();
}

function authldap_SiteLinkSet()
{
    authldap_IncludeSet();
    return new authldap_SiteLinkSet();
}

function authldap_UserSet()
{
    authldap_IncludeSet();
    return new authldap_UserSet();
}

function authldap_SiteOptionSet()
{
    authldap_IncludeSet();
    return new authldap_SiteOptionSet();
}

function authldap_NotifiedGroupsSet()
{
    authldap_IncludeSet();
    return new authldap_NotifiedGroupsSet();
}

function authldap_copyServerFromSite()
{
    global $babDB;
    
    $messages = array();
    
    $serverSet = authldap_ServerSet();
    $siteLinkSet = authldap_SiteLinkSet();
    $optionSet = authldap_SiteOptionSet();
    
    $sitesQuery = "SELECT id, name, ldap_admindn, ldap_adminpassword AS ldap_adminpassword, ldap_domainname, ldap_filter, ldap_attribute, ldap_encryptiontype, ldap_searchdn, ldap_host, ldap_decoding_type, ldap_userdn, authentification FROM ".BAB_SITES_TBL;
    $sitesResults = $babDB->db_query($sitesQuery);
    if( $babDB->db_num_rows($sitesResults) > 0 )
    {
        while($site = $babDB->db_fetch_assoc($sitesResults))
        {
            $serverRank = 0;
            if($site['authentification'] != 0){
                $serverRank ++;
                $server = $serverSet->newRecord();
                /*@var $server authldap_Server */

                //SERVER CONFIGURATION

                $server->id = $site['id'];
                $server->name = $site['name'];
                $server->active = true;
                $server->administratorDN = $site['ldap_admindn'];
                $server->administratorPassword = $site['ldap_adminpassword'];
                $server->domainName = $site['ldap_domainname'];
                $server->filter = $site['ldap_filter'];
                $server->passwordEncodeType = $site['ldap_encryptiontype'];
                $server->searchBase = $site['ldap_searchdn'];
                $server->serverAddress = $site['ldap_host'];
                $server->serverEncodeType = $site['ldap_decoding_type'];
                if($site['authentification'] == 1){
                    $server->serverType = authldap_ServerSet::SERVER_LDAP;
                    $server->login = $site['ldap_attribute'];
                }
                else if($site['authentification'] == 2){
                    $server->serverType = authldap_ServerSet::SERVER_AD;
                    $server->login = 'samaccountname';
                }
                $server->userDN = $site['ldap_userdn'];

                $ldapSiteQuery = "select id_field, x_name from ".BAB_LDAP_SITES_FIELDS_TBL.' where id_site="'.$babDB->db_escape_string($site['id']).'"';
                $ldapSiteResults = $babDB->db_query($ldapSiteQuery);

                //SERVER MAPPING

                $ldap = array();
                while($arr = $babDB->db_fetch_assoc($ldapSiteResults))
                {
                    $ldap[$arr['id_field']] = $arr['x_name'];
                }

                $mapping = bab_getDirectoriesFields(array(0));
                foreach ($mapping as $map){
                    if($serverSet->fieldExist($map['name'])){
                        $server->$map['name'] = $ldap[$map['id']];
                    }
                }

                if($server->save()){
                    $messages[] = sprintf(authldap_translate('Authentication parameters from the site %s have been recovered'), $site['name']).'<br/>';
                }
                else{
                    $messages[] = sprintf(authldap_translate('An error occured while attempting to recover the authentication parameters from the site %s'), $site['name']).'<br/>';
                }

                //USE SERVER

                $sitelinkRecord = $siteLinkSet->newRecord();

                $sitelinkRecord->site = $site['id'];
                $sitelinkRecord->rank = $serverRank;
                $sitelinkRecord->used = true;
                $sitelinkRecord->server = $server->id;

                $sitelinkRecord->save();
            }
            
            try{
                $ldapGenericConfiguration = authldap_getLdapGenericConfiguration();
                if(count($ldapGenericConfiguration) > 0){
                    $messages[] = authldap_translate('ldap_generic module found. Trying to recover configuration');
                }
            }
            catch(Exception $e){
                $messages[] = $e->getMessage();
            }
            
            //SITE OPTIONS
            
            $siteOption = $optionSet->newRecord();
            $siteOption->site = $site['id'];
            $siteOption->allowAdmin = true;
            $siteOption->save();
        }
    }
    
    return $messages;
}

/**
 * Get the ldap_generic module configuration if the module is installed
 * @return array 
 */
function authldap_getLdapGenericConfiguration()
{
    $ldapGeneric = bab_getAddonInfosInstance('ldap_generic');
    if(!$ldapGeneric){
        return array();
    }

    $ovldap_ini_file = $ldapGeneric->getPhpPath().'ovldap-ini.php';
       
    $arr_ini = parse_ini_file($ovldap_ini_file, true);

    if(count($arr_ini) == 0 )
    {
        throw new Exception(sprintf(authldap_translate('authldap_generic : Cannot read ini file %s', $ovldap_ini_file)));
    }
    if(empty($arr_ini['general']['password']) )
    {
        throw new Exception(authldap_translate('authldap_generic : Global password is empty'));
    }
    
    $registry = bab_getRegistryInstance();
    
    foreach($arr_ini as $section => $sectionconfig)
    {
        $registry->changeDirectory("/ldap_generic/$section");
        $I = $registry->getValueEx(array_keys($sectionconfig));

        /*@var $I bab_RegistryIterator */

        foreach($I as $arr)
        {
            $arr_ini[$section][$arr['key']] = $arr['value'];
        }
    }
    
    return $arr_ini;
    
}
