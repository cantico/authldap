<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";
require_once dirname(__FILE__).'/../functions.php';

global $babDB;

$LibOrm = bab_Functionality::get('LibOrm');
/*@var $LibOrm Func_LibOrm */

$LibOrm->initMysql();
ORM_MySqlRecordSet::setBackend(new ORM_MySqlBackend($babDB));

/**
 * @property ORM_IntField       $site
 * @property ORM_StringField    $group
 */
class authldap_NotifiedGroupsSet extends ORM_RecordSet
{
    
    public function __construct()
    {
        parent::__construct();

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_IntField('site')->setDescription('The id of the site the notified groups are for'),
            ORM_StringField('group')->setDescription('The group id')
        );
    }
    
    public function getRightsString($site)
    {
        $groups = $this->select($this->site->is($site));
        $output = array();
        
       foreach ($groups as $group)
        {
            $id_group = $group->group;
            if( $id_group >= BAB_ACL_GROUP_TREE )
            {
                $id_group -= BAB_ACL_GROUP_TREE;
                $output[] = ((string) $id_group).'+';
    
            } else {
                $output[] = (string) $group->group;
            }
        }
    
        return implode(',', $output);
    }
}


/**
 * @property ORM_IntField       $site
 * @property ORM_StringField    $group
 */
class authldap_NotifiedGroups extends ORM_Record
{
    
}