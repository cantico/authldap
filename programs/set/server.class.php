<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";
require_once dirname(__FILE__).'/../functions.php';
require_once $GLOBALS['babInstallPath'].'/utilit/dirincl.php';

global $babDB;

$LibOrm = bab_Functionality::get('LibOrm');
/*@var $LibOrm Func_LibOrm */

$LibOrm->initMysql();
ORM_MySqlRecordSet::setBackend(new ORM_MySqlBackend($babDB));

/**
 * @property ORM_IntField $serverType
 * @property ORM_StringField $name
 * @property ORM_BoolField $active
 * @property ORM_StringField $serverAddress
 * @property ORM_StringField $serverPort
 * @property ORM_StringField $searchBase
 * @property ORM_StringField $passwordEncodeType
 * @property ORM_StringField $administratorDN
 * @property ORM_StringField $administratorPassword
 * @property ORM_StringField $userDN
 * @property ORM_StringField $domainName
 * @property ORM_StringField $filter
 * @property ORM_StringField $serverEncodeType
 * @property ORM_StringField $login
 * 
 */
class authldap_ServerSet extends ORM_RecordSet
{
    
    const SERVER_LDAP = 0;
    const SERVER_AD = 1;
    
    public function __construct()
    {
        parent::__construct();

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('name'),
            ORM_IntField('serverType')->setDescription('The server type. 0 for LDAP, 1 for AD'),
            ORM_BoolField('active')->setDescription('Wether the server is active or not'),
            ORM_StringField('serverAddress'),
            ORM_StringField('serverPort'),
            ORM_StringField('searchBase'),
            ORM_StringField('passwordEncodeType')->setDescription('Only used when server type is LDAP'),
            ORM_StringField('administratorDN')->setDescription('Only used when server type is LDAP'),
            ORM_StringField('administratorPassword')->setDescription('Only used when server type is LDAP'),
            ORM_StringField('userDN')->setDescription('Only used when server type is LDAP'),
            ORM_StringField('domainName')->setDescription('Only used when server type is AD'),
            ORM_StringField('filter'),
            ORM_StringField('serverEncodeType'),
            ORM_StringField('login'),
            
			//SYNCHRO PARAMETERS
			ORM_BoolField('sync')->setDescription('Wether the server is synchronised or not'),
			ORM_StringField('sync_filter'),
			ORM_DatetimeField('last_synchronization'),
			
            ORM_BoolField('timer_active'),
            ORM_StringField('timer_hour'),
			ORM_StringField('logfile'),
			ORM_StringField('password'),
			ORM_BoolField('is_confirmed'),
			ORM_BoolField('disableusers'),
			ORM_BoolField('neverdisableadmin'),
			ORM_BoolField('neverdisableauth'),
			ORM_StringField('defaultemail'),
			
			ORM_StringField('userfield'),
			ORM_IntField('root'),
			ORM_BoolField('create_groups'),
			ORM_BoolField('remove_from_group'),
			ORM_BoolField('remove_from_groups_on_disable'),
			ORM_StringField('group_path_method'),
			
			ORM_StringField('groupsattribute'),
			ORM_IntField('grouproot'),
			
			ORM_TextField('recipients'),
            
            ORM_StringField('sync_login')->setDescription('admin login of the server to execute the synchronization'),
            ORM_StringField('sync_password')->setDescription('admin password of the server to execute the synchronization')
        );
        
        //MAPPING
        $dirFields = bab_getDirectoriesFields(array(0));
        foreach ($dirFields as $dirField){
            $this->addFields(
                ORM_StringField($dirField['name'])
            );
        }
    }

    public function getServerOrder($siteId = 1){
        $siteLinkSet = authldap_SiteLinkSet();
        $siteLinks = $siteLinkSet->select($siteLinkSet->site->is($siteId))->orderAsc($siteLinkSet->rank);
         
        $servers = array();
        $serversId = array();
        foreach ($siteLinks as $siteLink){
            $servers[] = $siteLink->server();
            $serversId[] = $siteLink->server()->id;
        }
         
        $serverSet = authldap_ServerSet();
        $otherServers = $serverSet->select($serverSet->id->notIn($serversId));
        foreach ($otherServers as $otherServer){
            $servers[] = $otherServer;
        }
         
        return $servers;
    }
}


/**
 * @property ORM_IntField $serverType
 * @property ORM_StringField $name
 * @property ORM_BoolField $active
 * @property ORM_StringField $serverAddress
 * @property ORM_StringField $searchBase
 * @property ORM_StringField $passwordEncodeType
 * @property ORM_StringField $administratorDN
 * @property ORM_StringField $administratorPassword
 * @property ORM_StringField $userDN
 * @property ORM_StringField $domainName
 * @property ORM_StringField $filter
 * @property ORM_StringField $serverEncodeType
 * @property ORM_StringField $login
 */
class authldap_Server extends ORM_Record
{
    public function ldapEncode($str, $type = null){
        global $babBody;
        $ovCharset = bab_charset::getDatabase();
    
        if (null === $type && isset($this->serverEncodeType)) {
            $type = $this->serverEncodeType;
        }
        $type = (int) $type;
    
        switch($type)
        {
            case BAB_LDAP_UTF8:
                if ('utf8' === $ovCharset) {
                    return $str; // utf8 to utf8
                } else {
                    return utf8_encode($str); // latin1 to utf8
                }
                break;
    
            case BAB_LDAP_ISO8859:
                if ('utf8' === $ovCharset) {
                    return utf8_decode($str); // utf8 to latin1
                } else {
                    return $str; // latin1 to latin1
                }
                break;
    
            default:
                trigger_error('Unsupported Charset');
                return $str;
                break;
        }
    }
    
    public function setValues(array $values)
    {
        parent::setValues($values);
        foreach ($values as $fieldName => $mixedValue) {
            if($values[$fieldName] == 'other'){
                if(isset($values[$fieldName.'Value']) && !empty($values[$fieldName.'Value'])){
                    $this->$fieldName = $values[$fieldName.'Value'];
                }
            }
        }
        return $this;
    }
    
    public function ldapDecode($str, $type = null)
    {
        $ovCharset = bab_charset::getDatabase();
    
        if (null === $type && isset($this->serverEncodeType)) {
            $type = $this->serverEncodeType;
        }
    
    
        $type = (int) $type;
    
        switch($type)
        {
            case BAB_LDAP_UTF8:
                if ('utf8' === $ovCharset) {
                    return $str; // utf8 to utf8
                } else {
                    return utf8_decode($str); // utf8 to latin1
                }
                break;
    
            case BAB_LDAP_ISO8859:
    
                if ('utf8' === $ovCharset) {
                    return utf8_encode($str); // latin1 to utf8
                } else {
                    return $str; // latin1 to latin1
                }
                break;
    
            default:
                trigger_error('Unsupported Charset');
                return $str;
                break;
        }
    
    }
    
    public function performSynchronization(LibTimer_eventHourly $event = null)
    {
        require_once dirname(__FILE__).'/../syncincl.php';
        ini_set('max_execution_time', '1200');
        	
        $sync = new authldap_Sync($this, $event);
        $sync->processEntries();
        $sync->close();
    }
    
}