<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";
require_once dirname(__FILE__).'/../functions.php';

global $babDB;

$LibOrm = bab_Functionality::get('LibOrm');
/*@var $LibOrm Func_LibOrm */

$LibOrm->initMysql();
ORM_MySqlRecordSet::setBackend(new ORM_MySqlBackend($babDB));

/**
 * @property ORM_IntField   $site
 * @property ORM_BoolField  $allowAdmin
 * @property ORM_BoolField  $notifyGroups
 * @property ORM_BoolField  $checkFields
 */
class authldap_SiteOptionSet extends ORM_RecordSet
{
    
    public function __construct()
    {
        parent::__construct();

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_IntField('site')->setDescription('The id of the site the options are for'),
            ORM_BoolField('allowAdmin')->setDescription('Allow administrators to log in if the LDAP/AD authentication fails'),
            ORM_BoolField('notifyGroups')->setDescription('Wether or not the selected groups are notified when a new user is created'),
            ORM_BoolField('checkFields')->setDescription('Wether or not the givenname, lastname and email are tested when creating a new user')
        );
    }
}


/**
 * @property ORM_IntField   $site
 * @property ORM_BoolField  $allowAdmin
 * @property ORM_BoolField  $notifyGroups
 * @property ORM_BoolField  $checkFields
 */
class authldap_SiteOption extends ORM_Record
{
    
}