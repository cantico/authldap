<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__).'/functions.php';

class Func_PortalAuthentication_AuthLdap extends Func_PortalAuthentication {
    
    public      $authenticatedUser      = null;
    public  	$errorMessages			= array();
    public      $server;
    public      $newUser                = false;
    
    public function getDescription()
    {
        return authldap_translate("Authentication functionality by LDAP and/or Active Directory");
    }
    
    public function getAuthenticationType()
    {
        return BAB_AUTHENTIFICATION_LDAP;
    }
    
    public function authenticateUserByLDAPOrAD($login, $password) {
        global $babBody;
        $babSite = $babBody->babsite;
        
        $userid = bab_getUserIdByNickname($login);
        $userSet = authldap_UserSet();
        
        $user = $userSet->get($userSet->user->is($userid)->_AND_($userSet->allow->is(1)));
        if($user){
            $AuthOvidentia = bab_functionality::get('PortalAuthentication/AuthOvidentia');
            /*@var $AuthOvidentia Func_PortalAuthentication_AuthOvidentia */
            $this->authenticatedUser = $AuthOvidentia->authenticateUserByLoginPassword($login, $password);
            // copy errors messages to original object
            $this->errorMessages = $AuthOvidentia->errorMessages;
        }
        
        $siteLinkSet = authldap_SiteLinkSet();
        $siteLinks = $siteLinkSet->select(
            $siteLinkSet->site->is($babSite['id'])
            ->_AND_($siteLinkSet->used->is(1))
        )->orderAsc($siteLinkSet->rank);
        foreach ($siteLinks as $siteLink){
            $this->clearErrors();
            $server = $siteLink->server();
            $this->server = $server;
            if(!isset($this->authenticatedUser)){
                switch ($server->serverType){
                    case authldap_ServerSet::SERVER_AD :
                        $this->authenticatedUser = $this->authenticateUserByActiveDirectory($login, $password, $babSite['id']);
                        break;
                    case authldap_ServerSet::SERVER_LDAP :
                        $this->authenticatedUser = $this->authenticateUserByLDAP($login, $password, $babSite['id']);
                        break;
                }
            }
        }
        return $this->authenticatedUser;
    }
    
    public function getAuthenticatedUser(){
        return $this->authenticatedUser;
    }
    
    public function logout(){
        bab_logout();
    }
    
    public function login(){
        $sLogin		= bab_pp('nickname', null);
        $sPassword	= bab_pp('password', null);
        $iLifeTime	= (int) bab_pp('lifetime', 0);
        
        if (!empty($sLogin) && !empty($sPassword))
        {
            bab_requireSaveMethod();
            $iIdUser = $this->authenticateUserByLDAPOrAD($sLogin, $sPassword);
            if ($this->userCanLogin($iIdUser))
            {
                $this->setUserSession($iIdUser, $iLifeTime);
                bab_createReversableUserPassword($iIdUser, $sPassword);
        
                return true;
            }
        
            if (bab_isAjaxRequest() && count($this->errorMessages) > 0) {
                header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
                echo bab_json_encode($this->getJsonErrors());
                die();
            }
        
        }
        else if ($sLogin === '' || $sPassword === '')
        {
            $this->addError(bab_translate("You must complete all fields !!"));
        }
        
        $url = authldap_Controller()->Login()->loginForm($this->loginMessage, implode("\n", $this->errorMessages));
		$url->location();
		
		return false;
    }
    
    public function authenticate()
    {
        $W = bab_Widgets();
        echo $this->getAuthForm()->display($W->HtmlCanvas());
        exit;
    }
    
    public function getAuthForm()
    {
        if(!isset($_REQUEST['referer']))
    	{
    		$referer = '';
    	}
    	else
    	{
    		$referer = $_REQUEST['referer'];
    	}
    
    	if (bab_rp('restricted')) {
    	    header($_SERVER['SERVER_PROTOCOL'].' 401 Unauthorized', true, 401);
    	}
    
    
    	$temp = new displayLogin_Template($referer);
    	$html =	bab_printTemplate($temp, 'authldap.login.html', 'login');
    
    	if (bab_isAjaxRequest()) {
    		echo $html;
    		die();
    	}
    
        $settings = bab_getInstance('bab_Settings');
        $site = $settings->getSiteSettings();
    
        $babBody = bab_getBody();
    
        $babBody->setTitle($title);
        $errors = explode("\n", $errorMessages);
        foreach ($errors as $errorMessage) {
            $babBody->addError($errorMessage);
        }
        $babBody->addItemMenu('signon', bab_translate("Login"), $GLOBALS['babUrlScript'].'?tg=login&cmd=signon');
    
        $babBody->setCurrentItemMenu('signon');
    
        if($site['registration'] == 'Y') {
            $babBody->addItemMenu('register', bab_translate("Register"), $GLOBALS['babUrlScript'].'?tg=login&cmd=register');
        }
    
        if(bab_isEmailPassword())
        {
            $babBody->addItemMenu('emailpwd', bab_translate("Lost Password"), $GLOBALS['babUrlScript'].'?tg=login&cmd=emailpwd');
        }
        bab_displayLoginPage($html, 'signon.html');
    }
    
    /**
     * Returns the user id for the specified nickname and password using the ldap backend.
     *
     * @param string	$login		The user nickname
     * @param string	$password	The user password
     * @return int		The user id or null if not found
     */
    protected function authenticateUserByLDAP($login, $password, $siteId)
    {
        global $babBody;

        include_once $GLOBALS['babInstallPath'] . 'utilit/ldap.php';
        $optionSet = authldap_SiteOptionSet();
        $options = $optionSet->get($optionSet->site->is($babBody->babsite['id']));
        $allowAdmin = isset($options) ? $options->allowAdmin : false;
        $notifyGroups = isset($options) ? $options->notifyGroups : false;

        $server = $this->server;
        
        $oLdap = new babLDAP($server->serverAddress, $server->serverPort, false);
        if (false === $oLdap->connect())
        {
            $this->addError(bab_translate("Connection failed. Please contact your administrator"));
            return null;
        }

        $aUpdateAttributes	= $this->getUpdateAttributes($siteId);
        
        $aAttributes		= array('dn', 'modifyTimestamp', $server->login, 'cn');
        while(list($key, $val) = each($aUpdateAttributes))
        {
            if(!in_array($key, $aAttributes))
            {
                $aAttributes[] = $key;
            }
        }
        if(!isset($aUpdateAttributes['sn']))
        {
            $aAttributes[] = 'sn';
        }
        if(!isset($aUpdateAttributes['mail']))
        {
            $aAttributes[] = 'mail';
        }
        
        if(!isset($aUpdateAttributes['givenname']))
        {
            $aAttributes[] = 'givenname';
        }
        
        $aExtraFieldId		= array();

//         bab_getLdapExtraFieldIdAndUpdateAttributes($aAttributes, $aUpdateAttributes, $aExtraFieldId);

        $bLdapOk = true;
        $aEntries = array();
        
        // LDAP
        if (isset($server->userDN) && !empty($server->userDN))
        {
            $sUserdn = str_replace('%UID', ldap_escapefilter($server->login), $server->userDN);
            $sUserdn = str_replace('%NICKNAME', ldap_escapefilter($login), $sUserdn);

            if (false === $oLdap->bind($server->ldapEncode($sUserdn), $server->ldapEncode($password)))
            {
                $this->addError(bab_translate("Binding failed. Please contact your administrator"));
                $bLdapOk = false;
            }
            else
            {
                $aEntries = $oLdap->search($server->ldapEncode($sUserdn), '(objectclass=*)');//, $aAttributes);
                if ($aEntries === false || $aEntries['count'] == 0)
                {
                    $this->addError(bab_translate("User not found in the directory"));
                    $bLdapOk = false;
                }
            }
        }
        else
        {
            $sFilter = '';
            if(isset($server->filter) && !empty($server->filter))
            {
                $sFilter = str_replace('%UID', ldap_escapefilter($server->login), $server->filter);
                $sFilter = str_replace('%NICKNAME', ldap_escapefilter($login), $sFilter);
            }
            else
            {
                $sFilter = "(|(".ldap_escapefilter($server->login)."=".ldap_escapefilter($login)."))";
            }

            $DnEntries = $oLdap->search($server->ldapEncode($server->searchBase), $server->ldapEncode($sFilter), array('dn'));

            if($DnEntries !== false && $DnEntries['count'] > 0 && isset($DnEntries[0]['dn']))
            {

                if(false === $oLdap->bind($DnEntries[0]['dn'], ldapEncode($password)))
                {
                    $this->addError(bab_translate("Binding failed. Please contact your administrator"));
                    $bLdapOk = false;
                } else {

                    // in some cases, the search is not allowed on all fields so a first search get the DN from search filter
                    // and the second search get the directory entry after the bind operation

                    $aEntries = $oLdap->search($DnEntries[0]['dn'], '(objectclass=*)', $aAttributes);
                }
            }
            else
            {
                $bLdapOk = false;
            }
        }


        $iIdUser = false;
        if (!isset($aEntries) || $aEntries === false)
        {
            $this->addError(bab_translate("Authentification failed. Please verify your login ID and your password"));
            $bLdapOk = false;
        }

        if( $bLdapOk )
        {
            $iIdUser = $this->registerUserIfNotExist($login, $password, $aEntries, $aUpdateAttributes);
            $this->addError('User '.$iIdUser.' trouvé sur le serveur'.$server->id);
            if (false === $iIdUser)
            {
                $oLdap->close();
                return null;
            }
            else
            {
                if ($aEntries['count'] > 0)
                {
                    bab_ldapEntryToOvEntry($oLdap, $iIdUser, $password, $aEntries, $aUpdateAttributes, $aExtraFieldId);
                    bab_ldapEntryGroups($iIdUser, $aEntries[0], $babBody->babsite['ldap_groups'], (bool) $babBody->babsite['ldap_groups_create']);
                }

                if($notifyGroups && $this->newUser)
                {
                    $sGivenname	= isset($aUpdateAttributes['givenname'])?$aEntries[0][$aUpdateAttributes['givenname']][0]:$aEntries[0]['givenname'][0];
                    $sSn		= isset($aUpdateAttributes['sn'])?$aEntries[0][$aUpdateAttributes['sn']][0]:$aEntries[0]['sn'][0];
                    $sMail		= isset($aUpdateAttributes['email'])?$aEntries[0][$aUpdateAttributes['email']][0]:$aEntries[0]['mail'][0];
                    $this->notifyAdminRegistration(bab_composeUserName($server->ldapDecode($sGivenname), $server->ldapDecode($sSn)), $server->ldapDecode($sMail), "");
                }
            }
        }

        $oLdap->close();

        if (false === $bLdapOk)
        {
            if($allowAdmin)
            {
                $bLdapOk = bab_haveAdministratorRight($login, $password, $iIdUser);
                if (false === $bLdapOk)
                {
                    $this->addError(bab_translate("Authentification failed. Please verify your login ID and your password"));
                }
            }
        }

        if (false !== $iIdUser && $bLdapOk)
        {
            $this->clearErrors();
            return $iIdUser;
        }
        return null;
    }
    

    protected function authenticateUserByActiveDirectory($login, $password, $siteId)
    {
        global $babBody;
        
        include_once $GLOBALS['babInstallPath'] . 'utilit/ldap.php';
        $optionSet = authldap_SiteOptionSet();
        $options = $optionSet->get($optionSet->site->is($babBody->babsite['id']));
        $allowAdmin = isset($options) ? $options->allowAdmin : false;
        $notifyGroups = isset($options) ? $options->notifyGroups : false;
        
        $server = $this->server;
        
        $oLdap = new babLDAP($server->serverAddress, $server->serverPort, false);
        if (false === $oLdap->connect())
        {
            $this->addError(bab_translate("Connection failed. Please contact your administrator"));
            return null;
        }

        $aUpdateAttributes	= $this->getUpdateAttributes($siteId);
        
        $aAttributes		= array('dn', 'modifyTimestamp', $server->login, 'cn');
        while(list($key, $val) = each($aUpdateAttributes))
        {
            if(!in_array($key, $aAttributes))
            {
                $aAttributes[] = $key;
            }
        }
        if(!isset($aUpdateAttributes['sn']))
        {
            $aAttributes[] = 'sn';
        }
        if(!isset($aUpdateAttributes['mail']))
        {
            $aAttributes[] = 'mail';
        }
        
        if(!isset($aUpdateAttributes['givenname']))
        {
            $aAttributes[] = 'givenname';
        }
        
        $aExtraFieldId		= array();

//         bab_getLdapExtraFieldIdAndUpdateAttributes($aAttributes, $aUpdateAttributes, $aExtraFieldId);

        $bLdapOk = true;
        $aEntries = array();

        //Active directory

        if (false === $oLdap->bind($server->ldapEncode($login."@".$server->domainName), $server->ldapEncode($password)))
        {
            $this->addError(bab_translate("Binding failed. Please contact your administrator"));
            $bLdapOk = false;
        }
        else
        {
            $sFilter = '';
            if (isset($server->filter) && !empty($server->filter))
            {
                $sFilter = str_replace('%NICKNAME', ldap_escapefilter($login), $server->filter);
            }
            else
            {
                $sFilter = "(|(samaccountname=".ldap_escapefilter($login)."))";
            }
            $aEntries = $oLdap->search($server->ldapEncode($server->searchBase), $server->ldapEncode($sFilter));//, $aAttributes);
        }
        

        if (!isset($aEntries) || $aEntries === false || (isset($aEntries['count']) && 0 === (int) $aEntries['count']))
        {
            $this->addError(bab_translate("Authentification failed. Please verify your login ID and your password"));
            $bLdapOk = false;
        }

        if( $bLdapOk )
        {
            $iIdUser = $this->registerUserIfNotExist($login, $password, $aEntries, $aUpdateAttributes);
            if (false === $iIdUser)
            {
                $oLdap->close();
                return null;
            }
            else
            {
                if ($aEntries['count'] > 0)
                {
                    bab_ldapEntryToOvEntry($oLdap, $iIdUser, $password, $aEntries, $aUpdateAttributes, $aExtraFieldId);
                    bab_ldapEntryGroups($iIdUser, $aEntries[0], $babBody->babsite['ldap_groups'], (bool) $babBody->babsite['ldap_groups_create']);
                }

                if($notifyGroups && $this->newUser)
                {
                    $sGivenname	= isset($aUpdateAttributes['givenname'])?$aEntries[0][$aUpdateAttributes['givenname']][0]:$aEntries[0]['givenname'][0];
                    $sSn		= isset($aUpdateAttributes['sn'])?$aEntries[0][$aUpdateAttributes['sn']][0]:$aEntries[0]['sn'][0];
                    $sMail		= isset($aUpdateAttributes['email'])?$aEntries[0][$aUpdateAttributes['email']][0]:$aEntries[0]['mail'][0];
                    $this->notifyAdminRegistration(bab_composeUserName($server->ldapDecode($sGivenname), $server->ldapDecode($sSn)), $server->ldapDecode($sMail), "");
                }

            }
        }
        $oLdap->close();

        if (false === $bLdapOk)
        {
            if ($allowAdmin)
            {
                $bLdapOk = bab_haveAdministratorRight($login, $password, $iIdUser);
                if( false === $bLdapOk)
                {
                    $this->addError(bab_translate("Authentification failed. Please verify your login ID and your password"));
                }
            }
        }

        if (false !== $iIdUser && $bLdapOk)
        {
            $this->clearErrors();
            return $iIdUser;
        }

        return null;
    }
    
    public function notifyAdminRegistration($name, $useremail, $warning){
        global $babBody;
        
        $mail = bab_mail();
        if( $mail == false ){
            return;
        }
        
        $mailBCT = 'mail'.$babBody->babsite['mail_fieldaddress'];
    
        require_once $GLOBALS['babInstallPath'].'admin/acl.php';
        $users = $this->getAccessUsers();
    
        foreach($users as $user){
            $mail->$mailBCT($user['email'], $user['name']);
        }
        $mail->mailFrom($babAdminEmail, $GLOBALS['babAdminName']);
        $mail->mailSubject(bab_translate("Inscription notification"));
    
        $emailInfos = new newUserEmailInfo($name, $useremail, $warning);
        $emailInfos->toHtml();
        $message = $mail->mailTemplate(bab_printTemplate($emailInfos,"mailinfo.html", "adminregistration"));
        $mail->mailBody($message, "html");
    
        $message = bab_printTemplate($emailInfos,"mailinfo.html", "adminregistrationtxt");
        $mail->mailAltBody($message);
        $mail->send();
    }
    
    /**
     * Return the list of the users who have the access right specified (table and id object)
     * Users disabled, non confirmed or invalid (validity_start & validity_end) are not selected
     * @return array :
     * array
     (
     [154] =>
     (
     [name] => Guillaume Dupont
     [firstname] => Guillaume
     [lastname] => Dupont
     [email] => test@test.com
     )
     )
     */
    protected function getAccessUsers() {
        global $babBody, $babDB;
    
        $groups = $this->getAccessGroups();
        $query = '';
    
        if (empty($groups)) {
            return array();
        } elseif (isset($groups[BAB_REGISTERED_GROUP]) || isset($groups[BAB_ALLUSERS_GROUP])) {
    
            $where = bab_userInfos::queryAllowedUsers(null, false, false);
    
            $query = 'SELECT `id`, `firstname`, `lastname`, `email`
					FROM '.BAB_USERS_TBL.' WHERE '.$where;
    
        } else {
    
            $where = bab_userInfos::queryAllowedUsers('u', false, false);
    
            $query = 'SELECT `u`.id,`u`.`firstname`, `u`.`lastname`,`u`.`email`
					FROM '.BAB_USERS_TBL.' `u`, '.BAB_USERS_GROUPS_TBL.' `g`
						WHERE `g`.`id_object`=`u`.`id` AND `g`.`id_group` IN('.$babDB->quote($groups).') AND '.$where;
        }    
    
        $user = array();
        if( !empty($query))
        {
            $res = $babDB->db_query($query);
            while ($arr = $babDB->db_fetch_assoc($res)) {
                $user[$arr['id']] = array(
                    'name' => bab_composeUserName($arr['firstname'],$arr['lastname']),
                    'firstname' => $arr['firstname'],
                    'lastname' => $arr['lastname'],
                    'email' => isset($arr['email']) ? $arr['email'] : false
                );
            }
        }
    
        return $user;
    }
    
    protected function getAccessGroups() {
    	global $babBody, $babDB;
    
    	$tree = new bab_grptree();
    	$groups = array();
    	
    	$res = $babDB->db_query("SELECT t.group, g.nb_groups FROM ".$babDB->backTick(authldap_NotifiedGroupsSet()->getTableName())." t left join ".BAB_GROUPS_TBL." g on g.id=t.group WHERE t.site='".$babDB->db_escape_string($babBody->babsite['id'])."'");
    	while ($arr = $babDB->db_fetch_assoc($res)) {
    		if ($arr['group'] >= BAB_ACL_GROUP_TREE ){
    			$arr['group'] -= BAB_ACL_GROUP_TREE;
    			$groups[$arr['group']] = $arr['group'];
    			$tmp = $tree->getChilds($arr['group'], true);
    			if( $tmp && is_array($tmp )){
    				foreach($tmp as $child) {
    					$groups[$child['id']] = $child['id'];
    				}
    			}
    		}
    		else {
    			if( $arr['nb_groups'] !== null ){
    				$rs = $babDB->db_query("select id_group from ".BAB_GROUPS_SET_ASSOC_TBL." where id_set=".$babDB->quote($arr['group']));
    				while( $rr = $babDB->db_fetch_array($rs)){
    					$groups[$rr['id_group']] = $rr['id_group'];
    				}
    			}
    			else {
    				$groups[$arr['group']] = $arr['group'];
    			}
    		}
    	}
    
    	return $groups;
	}
    
    public function addError($error){
        $this->errorMessages[] = $error;
    }
    
    public function getErrors(){
        return $this->errorMessages;
    }
    
    public function clearErrors()
    {
        $this->errorMessages = array();
    }
    
    /**
     * LDAP method to create the account
     * or update directory entry
     */
    private function registerUserIfNotExist($sNickname, $sPassword, $aEntries, $aUpdateAttributes)
    {
        global $babBody;
        $server = $this->server;
        
        $optionSet = authldap_SiteOptionSet();
        $options = $optionSet->get($optionSet->site->is($babBody->babsite['id']));
    
        //has to do it because keys and values are flipped
        $aUpdateAttributes = array_flip($aUpdateAttributes);
    
        $iIdUser = false;
        $aUser = bab_getUserByNickname($sNickname);
    
    
        $attribute_for_givenname	= isset($aUpdateAttributes['givenname']) 	? $aUpdateAttributes['givenname'] 	: 'givenname';
        $attribute_for_sn			= isset($aUpdateAttributes['sn']) 			? $aUpdateAttributes['sn']			: 'sn';
        $attribute_for_mn			= isset($aUpdateAttributes['mn'])			? $aUpdateAttributes['mn']			: '';
        $attribute_for_mail			= isset($aUpdateAttributes['email'])		? $aUpdateAttributes['email'] 		: 'mail';
    
    
        $test_fields = isset($options) ? $options->checkFields : false;
    
        if (isset($aEntries[0][$attribute_for_givenname][0])) {
            $sGivenname	= $server->ldapDecode($aEntries[0][$attribute_for_givenname][0]);
        } else if ($test_fields) {
            $this->addError(bab_translate('Error, registration of user is impossible, the givenname is missing'));
            return false;
        } else {
            $sGivenname = '';
        }
    
    
        if (isset($aEntries[0][$attribute_for_sn][0])) {
            $sSn	= $server->ldapDecode($aEntries[0][$attribute_for_sn][0]);
        } else if ($test_fields) {
            $this->addError(bab_translate('Error, registration of user is impossible, the lastname is missing'));
            return false;
        } else {
            $sSn = '';
        }
    
        if ($attribute_for_mn && isset($aEntries[0][$attribute_for_mn][0])) {
            $sMn	= $server->ldapDecode($aEntries[0][$attribute_for_mn][0]);
        } else {
            $sMn	= '';
        }
    
        if (isset($aEntries[0][$attribute_for_mail][0])) {
            $sMail	= $server->ldapDecode($aEntries[0][$attribute_for_mail][0]);
        } else if ($test_fields) {
            $this->addError(bab_translate('Error, registration of user is impossible, the email is missing'));
            return false;
        } else {
            $sMail = '';
        }
    
    
        if(is_null($aUser))
        {
    
            $this->newUser = true;
    
    
    
    
            $iIdUser = registerUser(
                $sGivenname,
                $sSn,
                $sMn,
                $sMail,
                $sNickname,
                $sPassword,
                $sPassword,
                true
                );
    
            if (!$iIdUser) {
                // msgerror should be set by the registerUser function
    
                $this->addError($babBody->msgerror);
                return false;
            }
        }
        else
        {
            $this->newUser = false;
            $iIdUser = $aUser['id'];
        }
        
        //UPDATE USER WITH VALUES FROM LDAP / AD
        if(!is_null($iIdUser)){
            $unformatedDirFields = $dirFields = bab_getDirectoriesFields(array(0));
            $dirFields = array();
            foreach ($unformatedDirFields as $dirField){
                $dirFields[] = $dirField['name'];
            }
            $ldapEntries = $aEntries[0];
            $userInfos = array();
            foreach ($dirFields as $dirField){
                if(isset($server->$dirField) && !empty($server->$dirField)){
                    $key = strtolower($server->$dirField);
                    $userInfos[$dirField] = $server->ldapDecode($aEntries[0][$key][0]);
                }
            }
            $error = '';
            $userHasBeenUpdated = bab_updateUserById($iIdUser, $userInfos, $error);
            if(strlen($error) > 0){
                $this->addError($error);
            }
        }
        
        return $iIdUser;
    }
    
    private function getUpdateAttributes($siteId)
    {
        global $babDB;
        
        $aUpdateAttributes = array();
        
        $oResult = $babDB->db_query('select sfrt.*, sfxt.id as idfx from ' . BAB_LDAP_SITES_FIELDS_TBL . ' sfrt left join ' . BAB_DBDIR_FIELDSEXTRA_TBL . ' sfxt on sfxt.id_field=sfrt.id_field where sfrt.id_site=\'' . $babDB->db_escape_string($siteId) . '\' and sfxt.id_directory=\'0\'');
        $iNumRows = $babDB->db_num_rows($oResult);
        $iIndex = 0;
        
        $aDatasReq1 = array();
        $aDatasReq2 = array();
        while($iIndex < $iNumRows && false !== ($aDatasReq1 = $babDB->db_fetch_array($oResult)))
        {
            $iIndex++;
            if($aDatasReq1['id_field'] < BAB_DBDIR_MAX_COMMON_FIELDS )
            {
                $aDatasReq2 = $babDB->db_fetch_array($babDB->db_query('select name, description from ' . BAB_DBDIR_FIELDS_TBL . ' where id=\'' . $babDB->db_escape_string($aDatasReq1['id_field']) . '\''));
                $sFieldName = $aDatasReq2['name'];
            }
            else
            {
                //			$aDatasReq2 = $babDB->db_fetch_array($babDB->db_query('select * from ' . BAB_DBDIR_FIELDS_DIRECTORY_TBL . ' where id=\'' . $babDB->db_escape_string(($aDatasReq1['id_field'] - BAB_DBDIR_MAX_COMMON_FIELDS)) . '\''));
                $sFieldName = 'babdirf' . $aDatasReq1['id'];
                $aExtraFieldId[$aDatasReq1['id']] = $aDatasReq1['idfx'];
            }
        
            if(!empty($aDatasReq1['x_name']))
            {
                $aUpdateAttributes[$aDatasReq1['x_name']] = mb_strtolower($sFieldName);
            }
        }
        
        return $aUpdateAttributes;
    }
    
    public function getConfigForm()
    {
        require_once dirname(__FILE__).'/ui/admin.ui.php';
        global $babBody;
        $babSite = $babBody->babsite;
        $siteId = $babSite['id'];
        $servers = authldap_ServerSet()->getServerOrder($siteId);
        $optionSet = authldap_SiteOptionSet();
        
        $options = $optionSet->get($optionSet->site->is($siteId));
        $options = isset($options) ? $options->getValues() : array();
        
        $form = new authldap_ConfigurationEditor($servers);
        $form->setHiddenValue('serverOrder[site]', $siteId);
        $form->setValues($options, array('serverOrder'));
        return $form;
    }
    
    public function getUserForm($userId, $pos = '', $grp = '')
    {
        require_once dirname(__FILE__).'/ui/admin.ui.php';
        $form = new authldap_UserEditor($userId, $pos, $grp);
        return $form;
    }
    
    public function isConfigured()
    {
        global $babBody;
        include_once $GLOBALS['babInstallPath'] . 'utilit/ldap.php';
        
        if (!function_exists('ldap_connect'))
        {
            $babBody->addError(bab_translate("You must have LDAP enabled on the server"));
            return false;
        }
        
        if (!function_exists('utf8_decode'))
        {
            $babBody->addError(bab_translate("You must have XML enabled on the server"));
            return false;
        }

        $babSite = $babBody->babsite;
        
        $set = authldap_SiteLinkSet();
        
        $siteLinks = $set->select($set->site->is($babSite['id']));
        foreach ($siteLinks as $siteLink){
            $server = $siteLink->server();
            
            $oLdap = new babLDAP($server->serverAddress, '', false);
            if (false === $oLdap->connect())
            {
                $babBody->addError(sprintf(authldap_translate("Connection failed on server %s"), $server->name));
                return false;
            }
        }
        
        return true;
    }
}

class newUserEmailInfo
{
    var $sitename;
    var $username;
    var $message;
    var $email;
    var $warning;


    function __construct($name, $useremail, $warning)
    {
        global $babSiteName;
        $this->email = $useremail;
        $this->username = $name;
        $this->sitename = $babSiteName;
        $this->warning = $warning;
        $this->message = bab_translate("Your site recorded a new registration on behalf of");
    }

    function toHtml() {
        $this->email = bab_toHtml($this->email);
        $this->username = bab_toHtml($this->username);
        $this->sitename = bab_toHtml($this->sitename);
        $this->warning = bab_toHtml($this->warning);
    }
}
