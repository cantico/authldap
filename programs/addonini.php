; <?php/*

[general]
name				="authldap"
version				="1.0.4"
addon_type			="EXTENSION"
addon_access_control=0
encoding			="UTF-8"
mysql_character_set_database="latin1,utf8"
description			="Authentification with LDAP or AD"
description.fr		="Authentification avec LDAP ou AD"
delete				=1
db_prefix			="authldap_"
ov_version			="8.6.96"
php_version			="5.2.0"
mysql_version		="4.0"
mod_ldap						="Available"
configuration_page				="main&idx=admin.configure"
author				="Cantico"


[addons]
widgets				="1.0.89"
LibOrm				="0.9.0"
LibTranslate		=">=1.12.0rc3.01"

[functionalities]


;*/ ?>