<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';
require_once dirname(__FILE__) . '/functions.php';

/**
 * Fonction appellée au moment de la mise à jour et de l'installation de l'addon
 */
function authldap_upgrade($version_base,$version_ini)
{
    global $babDB;
    
    require_once $GLOBALS['babInstallPath'] . 'utilit/upgradeincl.php';
    require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';
    require_once $GLOBALS['babInstallPath'] . "utilit/eventincl.php";
    require_once $GLOBALS['babInstallPath'] . 'utilit/functionalityincl.php';
    
    $serverSet = authldap_ServerSet();
    $siteLinkSet = authldap_SiteLinkSet();
    $userSet = authldap_UserSet();
    $optionSet = authldap_SiteOptionSet();
    $notifiedGroupsSet = authldap_NotifiedGroupsSet();
    
	$synchronize = new bab_synchronizeSql();
	$synchronize->addOrmSet($serverSet);
	$synchronize->addOrmSet($siteLinkSet);
	$synchronize->addOrmSet($userSet);
	$synchronize->addOrmSet($optionSet);
	$synchronize->addOrmSet($notifiedGroupsSet);

	$synchronize->updateDatabase();

	if(!isset($version_base) || empty($version_base)){
	    //If this is an installation, we copy the server configuration from the site configuration
	    $status = authldap_copyServerFromSite();
        foreach ($status as $msg){
            echo $msg;
        }
	}
	
	$addon = bab_getAddonInfosInstance('authldap');
	$addon->registerFunctionality('PortalAuthentication/AuthLdap', 'authentication.class.php');
	
	$addon->removeAllEventListeners();
	$addon->addEventListener('LibTimer_eventHourly', 'authldap_onHourly', 'events.php');

	return true;
}


/**
 * Fonction appellée au moment de la suppression de l'addon
 */
function authldap_onDeleteAddon()
{
    global $babDB;
    
    $AuthLdap = bab_functionality::get('PortalAuthentication/AuthLdap');
//     $authldapAuthType = $AuthLdap->getAuthenticationType();
    
    $addon = bab_getAddonInfosInstance('authldap');
    $addon->unregisterFunctionality('PortalAuthentication/AuthLdap');
	$addon->removeAllEventListeners();
	
	$AuthOvidentia = bab_functionality::get('PortalAuthentication/AuthOvidentia');
	$ovidentiaAuthType = $AuthOvidentia->getAuthenticationType();
	
	$sitesQuery = "SELECT id FROM ".BAB_SITES_TBL." WHERE authentification=".$babDB->quote($authldapAuthType);
	$sitesResults = $babDB->db_query($sitesQuery);
	if( $babDB->db_num_rows($sitesResults) > 0 )
	{
	    while($site = $babDB->db_fetch_assoc($sitesResults))
	    {
	        $query = "UPDATE ".BAB_SITES_TBL." SET authentification=".$babDB->quote($ovidentiaAuthType)." WHERE id=".$babDB->quote($site['id']);
	        $babDB->db_query($query);
	    }
	}
	
	
		
	return true;
}


?>