<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2011 by CANTICO ({@link http://www.cantico.fr})
 */
require_once $GLOBALS['babInstallPath'].'utilit/install.class.php';
require_once dirname(__FILE__).'/../ui/admin.ui.php';
require_once dirname(__FILE__).'/../ui/server.ui.php';


class authldap_CtrlAdmin extends authldap_Controller
{	
    public function configure()
	{
	    if(!bab_isUserAdministrator()){
	        throw new bab_AccessException(authldap_translate('Access denied to non administrators'));
	    }
		$W = bab_Widgets();
		$page = $W->BabPage();
		$page->addClass('widget-bordered');
		$page->addClass('BabLoginMenuBackground');
		$page->addClass('widget-centered');
		
		$serverSet = authldap_ServerSet();
		$servers = $serverSet->select();
		
    	$serverList = new authldap_ServerList($servers);

		$page->addItem($serverList);
		
		$page->setTitle(authldap_translate('Options'));

		return $page;
	}
	
	public function notifiedGroups()
	{
	    if(!bab_isUserAdministrator()){
	        throw new bab_AccessException(authldap_translate('Access denied to non administrators'));
	    }
	    global $babBody;
	    
	    $W = bab_Widgets();
	    $page = $W->BabPage();
	    $page->addClass('widget-bordered');
	    $page->addClass('BabLoginMenuBackground');
	    $page->addClass('widget-centered');
	    
	    $notifiedForm = new authldap_NotifiedGroupsEditor();
	    $notifiedForm->setValue(array('notifiedGroups', 'groups'), authldap_NotifiedGroupsSet()->getRightsString($babBody->babsite['id']));
	    $page->addItem($notifiedForm);
	    
	    $page->setTitle(authldap_translate('Notifications'));
	    
	    return $page;
	}
	
	public function saveNotifiedGroups(Array $notifiedGroups = null)
	{
	    if(!bab_isUserAdministrator()){
	        throw new bab_AccessException(authldap_translate('Access denied to non administrators'));
	    }
	    global $babBody;
	    $set = authldap_NotifiedGroupsSet();
	    $site = $notifiedGroups['site'];
	    
	    $set->delete($set->site->is($site));
	    
	    $groups = explode(',', $notifiedGroups['groups']);
	    foreach ($groups as $group){
	        $findPlus = strpos($group, '+');
	        if($findPlus !== false){
	            $group = str_replace('+', '', $group);
	            $group = intval($group, 10);
	            $group += BAB_ACL_GROUP_TREE;
	        }
	        $record = $set->newRecord();
	        $record->site = $site;
	        $record->group = $group;
	        $record->save();
	    }
	    
	    $babBody->addNextPageMessage(authldap_translate('Groups have been saved'));
	    authldap_Controller()->Admin()->notifiedGroups()->location();
	}
	
	public function saveOrder(Array $serverOrder = null)
	{
	    bab_requireSaveMethod();
	    if(!bab_isUserAdministrator()){
	        throw new bab_AccessException(authldap_translate('Access denied to non administrators'));
	    }
	    if(!isset($serverOrder['site']) || empty($serverOrder['site'])){
	        throw new bab_SaveErrorException(authldap_translate('The site must be specified'));
	    }
	    if(!isset($serverOrder['order']) || !is_array($serverOrder['order']) || empty($serverOrder['order'])){
	        throw new bab_SaveErrorException(authldap_translate('Invalid order'));
	    }
        
	    $siteLinkSet = authldap_SiteLinkSet();
	    $serverSet = authldap_ServerSet();
	    $optionSet = authldap_SiteOptionSet();
	    $serverIds = $serverOrder['order'];
	    
	    $serverLinkArray = array();
	    
	    $serverLinks = $siteLinkSet->select($siteLinkSet->server->in($serverIds)->_AND_($siteLinkSet->site->is($serverOrder['site'])));
	    foreach ($serverLinks as $serverLink){
	        $serverLinkArray[$serverLink->server] = $serverLink;
	    }
	    
	    //SAVE ORDER
	    $rank = 1;
	    foreach ($serverIds as $serverId){
	        
	        $serverRecord = $serverSet->get($serverSet->id->is($serverId));
	        if(!isset($serverRecord)){
	            throw new bab_SaveErrorException(sprintf(authldap_translate('The server id %s does not exist'), $serverId));
	        }
	        
	        $siteLink = isset($serverLinkArray[$serverRecord->id]) ? $serverLinkArray[$serverRecord->id] : $siteLinkSet->newRecord();
            /*@var $siteLink authldap_SiteLink */
            
            $siteLink->rank = $rank;
            $siteLink->server = $serverRecord->id;
            $siteLink->site = $serverOrder['site'];
            $siteLink->used = $serverOrder['used'][$serverRecord->id];
            
            $orderSaved = $siteLink->save();
                
	        $rank++;
	    }
	    
	    if(!$orderSaved){
	        die(authldap_translate('An error occured while saving the servers order'));
	    }
	    
	    //SAVE OPTIONS
	    $optionRecord = $optionSet->get($optionSet->site->is($serverOrder['site']));
	    if(!isset($optionRecord)){
	        $optionRecord = $optionSet->newRecord();
	        $optionRecord->site = $serverOrder['site'];
	    }
	    $optionRecord->allowAdmin = $serverOrder['allowAdmin'];
	    $optionRecord->notifyGroups = $serverOrder['notifyGroups'];
	    $optionRecord->checkFields = $serverOrder['checkFields'];
	    
	    $optionSaved = $optionRecord->save();
	    
	    if(!$optionSaved){
	        die(authldap_translate('An error occured while saving the server options'));
	    }
	    
	    die(authldap_translate('Server order and options has been saved'));
	}
	
	public function saveUser($user = null, $pos = null, $grp = null){
	    bab_requireSaveMethod();
	    if(!bab_isUserAdministrator()){
	        throw new bab_AccessException(authldap_translate('Access denied to non administrators'));
	    }
	    $set = authldap_UserSet();
	    $set->delete($set->user->is($user['user']));
	    
	    $record = $set->newRecord();
	    $record->user = $user['user'];
	    $record->allow = $user['allow'];
	    
	    $record->save();
	    
	    header('Location: ' . $GLOBALS['babUrlScript'] . '?tg=users&idx=List&pos=' . $pos . '&grp=' . $grp);
	}
}