<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2011 by CANTICO ({@link http://www.cantico.fr})
 */
require_once $GLOBALS['babInstallPath'].'utilit/install.class.php';
require_once dirname(__FILE__).'/../ui/server.ui.php';

class authldap_CtrlServer extends authldap_Controller
{	
	public function edit($id = null)
	{
	    if(!bab_isUserAdministrator()){
	        throw new bab_AccessException(authldap_translate('Access denied to non administrators'));
	    }
	    $W = bab_Widgets();
	    $page = $W->BabPage();
	
	    $editor = new authldap_ServerEditor();
	    $page->setTitle(authldap_translate('Create a new server'));
	    if(isset($id)){
	        $page->setTitle(authldap_translate('Edit server'));
	    }
	    
	    if(isset($id)){
	        $set = authldap_ServerSet();
	        $server = $set->get($set->id->is($id));
	        if(count($server) > 0){
                $editor->setValues(
                    $server->getValues(), 
                    array('serverOptions')
                );
                $editor->addItem(
                    $W->Link(
        		        authldap_translate('Launch synchronization in a popup'), 
        		        $this->proxy()->performSynchronization($server->id)
                    )->setOpenMode(Widget_Link::OPEN_POPUP)
                );
	        }
	        else{
	            throw new bab_AccessException(authldap_translate('This server does not exist'));
	        }
	    }
	    $editor->setValidateAction($this->proxy()->validateSaveServer());
	    $page->addItem($editor);
	    $addon = bab_getAddonInfosInstance('authldap');
	    $page->addJavascriptFile($addon->getTemplatePath() . 'identicalPasswords.js');
	    return $page;
	}
	
	public function performSynchronization($id = null){
	    if(!bab_isUserAdministrator()){
	        throw new bab_AccessException(authldap_translate('Access denied to non administrators'));
	    }
	    if(!isset($id) || empty($id)){
	        throw new bab_AccessException(authldap_translate('Server id is missing'));
	    }
	    
	    $serverSet = authldap_ServerSet();
	    $server = $serverSet->get($serverSet->id->is($id));
	    
	    if(empty($server)){
	        throw new bab_AccessException(authldap_translate('Unknown server'));
	    }
	    
	    $server->performSynchronization();
	    die();
	}
	
	public function validateSaveServer(Array $serverOptions = null)
	{
	    echo bab_json_encode($this->checkValidateServer($serverOptions));
	    die();
	}
	
	private function checkValidateServer($serverOptions)
	{
	    authldap_IncludeSet();
	    $set = authldap_ServerSet();
	    
	    $errors = array();
	    
	    if(!isset($serverOptions['serverType']) || ($serverOptions['serverType'] != authldap_ServerSet::SERVER_LDAP && $serverOptions['serverType'] != authldap_ServerSet::SERVER_AD)){
	        $errors[] = array('origin' => 'serverType', 'message' => authldap_translate('The server type is mandatory'));
	    }
	    else{
	        if($serverOptions['serverType'] == authldap_ServerSet::SERVER_LDAP){
	            $differentPasswords = true;
	            if(isset($serverOptions['administratorPassword'][1]) && isset($serverOptions['administratorPassword'][2])){
	                if($serverOptions['administratorPassword'][1] === $serverOptions['administratorPassword'][2]){
	                    $differentPasswords = false;
	                }
	            }
	            if($differentPasswords){
	                $errors[] = array('origin' => 'passwords', 'message' => authldap_translate('Passwords must be identicals'));
	            }
	        }
	        else if($serverOptions['serverType'] == authldap_ServerSet::SERVER_AD){
	            if(!isset($serverOptions['domainName']) || empty($serverOptions['domainName'])){
	                $errors[] = array('origin' => 'domainName', 'message' => authldap_translate('The domain name is mandatory'));
	            }
	        }
	    }
	    
	    $mandatoryFields = array(
	        'name' => authldap_translate('Server name'),
	        'serverAddress' => authldap_translate('Server address'),
	        'login' => authldap_translate('Login'),
	        'sn' => authldap_translate('Lastname'),
	        'givenname' => authldap_translate('Firstname'),
	        'email' => authldap_translate('Email')
	    );
	    foreach ($mandatoryFields as $field => $label){
	        if(!isset($serverOptions[$field]) || empty($serverOptions[$field])){
	            if($field == 'login' && $serverOptions['serverType'] == authldap_ServerSet::SERVER_AD){
	                
	            }
	            else{
	                $errors[] = array('origin' => $label, 'message' => sprintf(authldap_translate('%s is mandatory'), $label));
	            }
	        }
	        else if($serverOptions[$field] == 'other' && (!isset($serverOptions[$field.'Value']) || empty($serverOptions[$field.'Value']))){
	            $errors[] = array('origin' => $label, 'message' => sprintf(authldap_translate('%s is mandatory'), $label));
	        }
	    }

	    if(isset($serverOptions['serverId'])){
	        $otherServerWithName = $set->select($set->name->is($serverOptions['name'])->_AND_($set->id->isNot($serverOptions['serverId'])));
	        if(count($otherServerWithName) > 0){
	            $errors[] = array('origin' => 'serverName', 'message' => authldap_translate('A server with this name already exists'));
	        }
	    }
	    return $errors;
	}
	
	public function saveServer(Array $serverOptions = null)
	{
	    bab_requireSaveMethod();
	    if(!bab_isUserAdministrator()){
	        throw new bab_AccessException(authldap_translate('Access denied to non administrators'));
	    }
	    
	    authldap_IncludeSet();
        
        $errors = $this->checkValidateServer($serverOptions);
        foreach ($errors as $message){
            throw new bab_SaveErrorException($message['message']); 
        }
        
        $serverOptions['administratorPassword'] = $serverOptions['administratorPassword'][1];
        
        $set = authldap_ServerSet();
        

        if(isset($serverOptions['serverId'])){
            $otherServerWithName = $set->select($set->name->is($serverOptions['name'])->_AND_($set->id->isNot($serverOptions['serverId'])));
            $server = $set->get($set->id->is($serverOptions['serverId']));
            if(count($server) == 0){
                $server = $set->newRecord();
            }
        }
        else{
            $server = $set->newRecord();
        }
        
        $server->setValues($serverOptions);
        if($server->serverType == authldap_ServerSet::SERVER_AD){
            $server->login = 'samaccountname';
        }
        
        $babBody = bab_getBody();
        if ($server->save()) {
            $babBody->addNextPageMessage(authldap_translate('The server has been saved successfully'));
        } else {
            $babBody->addNextPageError(authldap_translate('An error occured while saving the server'));
        }
        
        authldap_Controller()->Admin()->configure()->location();
	}
}
