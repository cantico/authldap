<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once $GLOBALS['babInstallPath']."utilit/delincl.php";
require_once dirname(__FILE__).'/message.class.php';


class authldap_Sync
{
	/**
	 * output message to LibTimer log, file log, screen
	 * @var bool
	 */
	public $debug = true;

	/**
	 * Ldap link
	 * @var ressource
	 */
	public $link;

	/**
	 * Log file ressource (can be null)
	 * @var unknown_type
	 */
	public $logfd;

	/**
	 * can be null
	 * @var LibTimer_Event
	 */
	public $timer;

	/**
	 *
	 * @var array
	 */
	public $usrattributes;

	/**
	 *
	 * @var array
	 */
	public $users_exclude;

	/**
	 *
	 * @var array
	 */
	public $babGeneralGroups = array();


	public $arrldapusers = array();

	/**
	 * List of created user id
	 * @var int[]
	 */
	public $arrfidusers = array();


	/**
	 * Notifications messages
	 * @var array
	 */
	protected $messages = array();
	
	
	protected $server = null;


	public function __construct(authldap_Server $server, LibTimer_event $timer = null)
	{
		$this->server = $server;
		$this->timer = $timer;

		if (!empty($server->logfile))
		{
			$logfile = sprintf($server->logfile, $server->name);

			$this->logfd = @fopen($logfile, "a");
			if( !$this->logfd )
			{
				$this->logfd = null;

				if (!isset($this->timer))
				{
					echo "Cannot create log file ".$logfile."<br />";
				}
			}
		}

		
		$serverAddress = $server->serverAddress;
		$serverPort = $server->serverPort;
		$servArray = explode(':', $server->serverAddress);
		if (isset($servArray[1])) {
			$serverAddress = $servArray[0];
			$serverPort = $servArray[1];
		}

		$serverPort = intval($serverPort);
		
		$this->link = ldap_connect($serverAddress, $serverPort);
		if( $this->link === false )
		{
			$this->writeLog("error", "Cannot connect to ldap server : " . $server->name);
			$this->close();
		}
		
		/**TODO**/
		$options = array(
			'LDAP_OPT_PROTOCOL_VERSION' => 3,
			'LDAP_OPT_REFERRALS' => 0
		);
		
		foreach ($options as $optK => $optV) {
			if( $optV !== '' && !empty($optK)) {
				@ldap_set_option($this->link,constant($optK),$optV);
			}
		}

		$bindn = $server->sync_login;
		$password = $server->sync_password;

		if( !empty($bindn))
		{
			$ret = ldap_bind($this->link, $bindn, $password);
			if( $ret === false )
			{
				$this->writeLog("error", "Cannot bind to ldap server : " . $server->name);
				$this->close();
			}
		}




		$attributes = authldap_getAttributes($this->server);
		$mapping = authldap_getDirectoryMapping($this->server);

		$this->usrattributes = $attributes;

		if( count($mapping))
		{
			reset($mapping);
			while(list($key, $val) = each($mapping))
			{
				if( !empty($val) && !empty($key))
				{
					if( !in_array($val, $this->usrattributes))
					{
						$this->usrattributes[] = $val;
					}
				}
			}
		}


		if (!empty($server->userfield))
		{
			$this->usrattributes[] = $server->userfield;
		}

		if( isset($server->groupsattribute) && !empty($server->groupsattribute) )
		{
			$this->usrattributes[] = $server->groupsattribute;
		}

		$this->users_exclude = array();
		//TODO or exlude par filter
		/*if( isset($arr_ini['exclude_users']) && count($arr_ini['exclude_users']))
		{
			reset($arr_ini['exclude_users']);
			while(list($key, $val) = each($arr_ini['exclude_users']))
			{
				if( !empty($key) && !in_array($val, $this->usrattributes))
				{
					$this->usrattributes[] = $val;
					$this->users_exclude[$key] = $val;
				}
			}
		}*/

	}




	protected function addMessage(authldap_Message $message)
	{
	    $key = get_class($message);

	    if (!isset($this->messages[$key])) {
	        $this->messages[$key] = array();
	    }

	    $this->messages[$key][] = $message;
	}


	/**
	 *
	 * @param string $type
	 * @param string $errortxt
	 */
	public function writeLog($type, $errortxt)
	{
		if( !$this->debug )
			return;

		$datetime = sprintf("%s/%s/%s", date('d'), date('m'), date('Y'));
		$tmp = sprintf("[%s %s-%s-%s] [%s] %s\r\n", $datetime, date('H'), date('i'), date('s'), $type, $errortxt);

		if(isset($this->logfd))
		{
			fputs($this->logfd, $tmp);
		}

		if (isset($this->timer))
		{
			$this->timer->log('ldap_generic', $tmp);
		} else {
			echo $tmp."<br>";
		}
	}


	/**
	 *
	 */
	public function close()
	{
		if( isset($this->logfd) )
			{
			fclose($this->logfd);
		}

		if( $this->link !== false )
			{
			ldap_close($this->link);
		}

		return;
	}




	/**
	 * @param array $entry
	 * @return array
	 */
	protected function getGroupsFromAttribute(Array $entry)
	{
	    $attribute = $this->server->groupsattribute;

	    $groups = array();

	    if (!isset($entry[$attribute])) {
	        return $groups;
	    }

	    if( is_array($entry[$attribute])) {
	        for( $mk = 0; $mk < $entry[$attribute]['count']; $mk++) {
	            $groups[] =  authldap_decode($entry[$attribute][$mk], $this->server->serverEncodeType);
	        }
	    }
	    else {
	        $groups[] =  authldap_decode($entry[$attribute], $this->server->serverEncodeType);
	    }

	    return $groups;
	}



	/**
	 * @return array
	 */
	public function getEntries()
	{

		/* recherche des utilisateurs dans LDAP */
		$usersbasedn = $this->server->searchBase;

		$res = ldap_search($this->link, $usersbasedn,  $this->server->sync_filter, $this->usrattributes);

		if( $res !== false )
		{
			$entries = ldap_get_entries($this->link, $res);
			if( is_array($entries))
			{
				return $entries;

			} else {
				$this->writeLog("info", "Nombre d'utilisateurs ajoutes dans Ovidentia : 0");
				$this->close();
			}
		} else
		{
			$this->writeLog("error", "Echec de la recherche LDAP");
			$this->close();
		}
	}


	public function processEntries()
	{
		$this->writeLog("info", '============================================================');
		$this->writeLog("info", sprintf("Synchronisation du server : %s", $this->server->name));
		$entries = $this->getEntries();
		$count = $entries['count'];
		for ( $i = 0; $i < $count; $i++ ) {
			$this->processEntry($entries[$i]);
		}

		$this->disableUsers();

		$inserted = count($this->arrfidusers);
		$this->writeLog("info", sprintf("Nombre d'utilisateurs ajoutes dans Ovidentia : %d", $inserted));

		// record last synchronization date
		$this->server->last_synchronization = date('Y-m-d H:i:s');
		$this->server->save();

	}


	/**
	 * Disable a user
	 * @param int $iduser
	 * @param string $nickname
	 * @param string $lastname
	 * @param string $firstname
	 */
	protected function disableUser($iduser, $nickname, $lastname, $firstname)
	{
	    $error = '';
	    if (bab_updateUserById($iduser, array('disabled' => 1), $error))
	    {
	        $this->writeLog("info", "Disabled user: ".$firstname." ".$lastname. " (".$nickname.")");

	        $message = new authldap_DisableUserMessage();

	        $message->iduser = $iduser;
	        $message->firstname = $firstname;
	        $message->lastname = $lastname;
	        $message->nickname = $nickname;

	        $this->addMessage($message);

	    } else {
	        $this->writeLog("error", $error);
	    }
	}


	/**
	 * Register new user
	 * @return int
	 */
	protected function registerUser($firstname, $lastname, $middlename, $email, $nickname)
	{
	    $error = '';

	    $iduser = bab_registerUser( $firstname, $lastname, $middlename, $email, $nickname, $this->server->password, $this->server->password, $this->server->is_confirmed, $error);
	    if ($iduser)
	    {
	        $this->arrfidusers[] = $iduser;
	        $this->writeLog("info", "Utilisateur ajoute dans la base d'Ovidentia: ".$firstname." ".$lastname. " (".$nickname.")");
	        $message = new authldap_RegisterUserMessage();

	        $message->iduser = $iduser;
	        $message->firstname = $firstname;
	        $message->lastname = $lastname;
	        $message->nickname = $nickname;

	        $this->addMessage($message);

	        return $iduser;
	    }


        $this->writeLog("warn", "Erreur d'insertion dans la base Ovidentia[".$error."]: ".$firstname." ".$lastname. " (".$nickname.")");
	    return 0;
	}



	/**
	 * Process one entry from the LDAP server
	 */
	protected function processEntry(Array $entry)
	{
		$mapping = authldap_getDirectoryMapping($this->server);

		$bexcludeuser = false;
		if( count($this->users_exclude))
		{
			foreach($this->users_exclude as $key => $val )
			{
				if( isset($entry[$key][0]) && $val = trim(authldap_decode($entry[$key][0], $this->server->serverEncodeType)) )
				{
					$bexcludeuser = true;
					break;
				}
			}
		}

		if( 
		    isset($entry[$mapping['login']][0]) && 
		    !empty($entry[$mapping['login']][0]) &&  
		    isset($entry[$mapping['givenname']][0]) && 
		    !empty($entry[$mapping['givenname']][0]) && 
		    isset($entry[$mapping['sn']][0]) && 
		    !empty($entry[$mapping['sn']][0]))
		{
			$nickname = trim(authldap_decode($entry[$mapping['login']][0], $this->server->serverEncodeType));
			$lastname = trim(authldap_decode($entry[$mapping['sn']][0], $this->server->serverEncodeType));
			$firstname = trim(authldap_decode($entry[$mapping['givenname']][0], $this->server->serverEncodeType));

			$iduser = bab_getUserIdByNickname($nickname);
			if( $bexcludeuser )
			{
				if( $iduser )
				{
					$this->disableUser($iduser, $nickname, $lastname, $firstname);
				}
				return;
			}

			if( isset($mapping['email']) && isset($entry[$mapping['email']][0]))
			{
				$email = trim(authldap_decode($entry[$mapping['email']][0], $this->server->serverEncodeType));
			}
			else
			{
				$email = '';
			}

			if (empty($email) && isset($this->server->defaultemail))
			{
				$email = $this->server->defaultemail;
			}


			if( isset($mapping['middlename']) && isset($entry[$mapping['middlename']][0]))
			{
				$middlename = trim(authldap_decode($entry[$mapping['middlename']][0], $this->server->serverEncodeType));
			}
			else
			{
				$middlename = '';
			}

			if( $iduser <= 0 )
			{
				$iduser = $this->registerUser($firstname, $lastname, $middlename, $email, $nickname);
			}


			if( $iduser > 0 )
			{
				$this->arrldapusers[$iduser] = $iduser;
				/* Initialisation des parametres utilisateur */
				$info = array();

				$info['sn'] = $lastname;
				$info['givenname'] = $firstname;
				$info['middlename'] = $middlename;
				$info['email'] = $email;
				$info['disabled'] = 0; // force enable, if disabled by a previous sync

				if( count($mapping))
				{

					reset($mapping);
					while(list($key, $val) = each($mapping))
					{
						// do not decode binary string
					    if ($key == 'jpegphoto' && isset($entry[$val][0]))
						{
							$info[$key] = $entry[$val][0];
							continue;
						}

						if( !empty($val) && !empty($key) && isset($entry[$val][0]))
						{
							$info[$key] = trim(authldap_decode($entry[$val][0], $this->server->serverEncodeType));
						}
					}
				}

				/* Mise a jour des donnees de l'utilisateur */
				/**/
				$error = '';
				if(!bab_updateUserById($iduser, $info, $error))
				{
					$this->writeLog("error", $error);
				} else {
					$this->writeLog("info", "Utilisateur mis a jour dans la base d'Ovidentia: ".$firstname." ".$lastname. " (".$nickname.")");
				}

				if (!empty($this->server->userfield) )
				{
					$memberOfProp = strtolower($this->server->userfield);

					if (isset($entry[$memberOfProp]))
					{
						$memberOf = $entry[$memberOfProp];
						unset($memberOf['count']);
						$this->processUserLdapGroups($memberOf, $iduser);
					}
				}


				if( !empty($this->server->groupsattribute) && isset($this->server->groupsattribute) && !empty($this->server->grouproot))
				{
					$groups = $this->getGroupsFromAttribute($entry);

					for ( $mk = 0; $mk < count($groups); $mk++ ) {

						$idngrp = authldap_getLevelGoupId($this->server->grouproot, $groups[$mk]);
						if( $idngrp === 0 ) {
							$idngrp = bab_createGroup($groups[$mk], '', 0, $this->server->grouproot);
							if( $idngrp ) {
							   $this->writeLog("info", "Groupe ajoute dans la base d'Ovidentia: ".$groups[$mk]);
							}
						}

						if( $idngrp )
						{
						    $this->attachUser($idngrp, $iduser, $groups[$mk]);
						}
					}
				}
			}
		}
		else
		{
			$nom = isset($entry[$mapping['sn']][0])? authldap_decode($entry[$mapping['sn']][0], $this->server->serverEncodeType): "";
			$prenom = isset($entry[$mapping['givenname']][0])? authldap_decode($entry[$mapping['givenname']][0], $this->server->serverEncodeType): "";
			$uid = isset($entry[$mapping['login']][0])? authldap_decode($entry[$mapping['login']][0], $this->server->serverEncodeType): "";
			$this->writeLog("warn", "Donnees utilisateur incompletes : Nom='".$nom."' Prenom='".$prenom."'"." uid='".$uid."'");
		}
	}

	/**
	 *  on desactive les utilisateurs Ovidentia non presents dans le serveur LDAP
	 */
	public function disableUsers()
	{
		global $babDB;

		$disableusers 		= (1 === (int) $this->server->disableusers);
		$neverdisableadmin	= (1 === (int) $this->server->neverdisableadmin);
		$neverdisableauth	= (1 === (int) $this->server->neverdisableauth);
		$removefromgroups   = (1 === (int) $this->server->remove_from_groups_on_disable);

		if($disableusers && count($this->arrldapusers) > 0 )
		{
			$resgrp = $babDB->db_query("select id, firstname, lastname, nickname, db_authentification, disabled from ".BAB_USERS_TBL."");
			while( $arr = $babDB->db_fetch_array($resgrp))
			{
				if( !isset($this->arrldapusers[$arr['id']] ))
				{

					if ($neverdisableadmin && bab_isMemberOfGroup(BAB_ADMINISTRATOR_GROUP, $arr['id']))
					{
						continue;
					}

					if ($neverdisableauth && 'Y' === $arr['db_authentification'])
					{
						continue;
					}

					if ($removefromgroups) {
					    $this->removeFromGroups($arr['id']);
					}

					if ($arr['disabled']) {
					    continue;
					}

					$this->disableUser($arr['id'], $arr['nickname'], $arr['lastname'], $arr['firstname']);

				}
			}
		}
	}



	/**
	 * Remove user from group under the root group
	 * @param int $id_user
	 */
	private function removeFromGroups($id_user)
	{
	    $id_group = (int) $this->server->root;

	    if (0 === $id_group) {
	        // not configured
	        return;
	    }

	    $g = bab_getGroups($id_group, true);
	    $u = bab_getUserGroups($id_user);

	    $groups = array_intersect($g['id'], $u['id']);

	    foreach($groups as $id_group) {
	        bab_removeUserFromGroup($id_user, $id_group);
	    }
	}








	/**
	 *
	 * @param array $dn_list
	 * @param int $id_user
	 */
	public function processUserLdapGroups($dn_list, $id_user)
	{
		$processed_groups = array();

		foreach($dn_list as $dn)
		{
			$dn = authldap_decode($dn, $this->server->serverEncodeType);
			$id_group = $this->processUserLdapGroup($dn, $id_user);
			if ($id_group > 0)
			{
				$processed_groups[$id_group] = $id_group;
			}
		}

		// remove user from groups

		if (isset($this->server->remove_from_group) && 1 === (int) $this->server->remove_from_group)
		{
			$current_groups = authldap_getUserGroupsUnderRoot($id_user, $this->server);
			foreach($current_groups as $id_cgroup => $groupname)
			{
				if (!isset($processed_groups[$id_cgroup]))
				{
					bab_detachUserFromGroup($id_user, $id_cgroup);
					$this->writeLog("info", sprintf("The user %s, has been removed from group %s", bab_getUserName($id_user), $groupname));
				}
			}
		}
	}



	/**
	 * Get groups path in array using the dn value
	 * @param string $dn One group dn from the list of group found in memberOf
	 *
	 * @return array
	 */
	private function getLdapGroupPath_dn($dn)
	{
	    $ovgroups = array();
	    foreach(explode(',', $dn) as $pair) {

	        list(, $groupname) = explode('=', $pair);
	        array_unshift($ovgroups, $groupname);

	        if (authldap_getRootGroupName($this->server) === $groupname) {
	            return $ovgroups;
	        }
	    }

	    return null;
	}

	/**
	 * Get groups path in array using the memberof attribute
	 * This does not work if no memberof on the ldap group
	 *
	 * @param string $dn One group dn from the list of group found in memberOf
	 *
	 * @return array
	 */
	private function getLdapGroupPath_memberof($dn)
	{
	    try {
	        $list = authldap_getLdapGroupAncestors($this->link, $dn, array(), $this->server->serverEncodeType);

	    } catch(Exception $e)
	    {
	        $this->writeLog("error", $e->getMessage());
	        return 0;
	    }

	    $ovgroups = array();

	    foreach($list as $group)
	    {
	        if (empty($ovgroups) && authldap_getRootGroupName($this->server) !== $group['cn'][0]) {
	            continue;
	        }

	        $ovgroups[] = $group['cn'][0];
	    }

	    if (empty($ovgroups)) {
	        return null;
	    }

	    return $ovgroups;
	}


	/**
	 * process group of user
	 * If the group is not under the root group, return 0
	 *
	 * @param string $dn
	 *
	 * @return int id of the group (created or modified)
	 */
	public function processUserLdapGroup($dn, $id_user)
	{
	    global $babBody;

		switch($this->server->group_path_method) {
		    case 'memberof':
		        $ovgroups = $this->getLdapGroupPath_memberof($dn);
		        break;

		    case 'dn':
	            $ovgroups = $this->getLdapGroupPath_dn($dn);
	            break;

		    default:
		        throw new Exception('wrong value for group_path_method');
		}


	    if (!isset($ovgroups))
	    {
	        // group ignored because the root group does not exists in path
	        $this->writeLog("info", 'Ignored group : '.$dn);
	        return 0;
	    }

	    $this->writeLog("info", 'LDAP GROUP : '.implode(' > ', $ovgroups));



		$id_parent = authldap_getRootGroupParentId($this->server);
		foreach($ovgroups as $groupname)
		{
			$id_group = authldap_getLevelGoupId($id_parent, $groupname);
			if (0 === $id_group) {
				if (empty($this->server->create_groups)) {
					$this->writeLog("error", sprintf("failed to create the group %s, group creation is disabled", $groupname));
					return 0;
				}


				if ($id_parent == authldap_getRootGroupParentId($this->server) && authldap_getRootGroupName($this->server) !== $groupname)
				{
					die('error '.$groupname);
				}

				$id_group = bab_createGroup($groupname, '', 0, $id_parent);

				if (!$id_group) {
				    $this->writeLog("error", 'Error on group '.$groupname.' : '.$babBody->msgerror);
				    return 0;

				} else {
				    $this->writeLog("info", sprintf("The group %s has been created", $groupname));
				}
			}

			$id_parent = $id_group;
		}



		// attach user to the last group in list
		$this->attachUser($id_group, $id_user, $groupname);

		return $id_group;
	}


	/**
	 * Attach user to group with message in log
	 */
	protected function attachUser($id_group, $id_user, $groupname)
	{
	    if (!bab_isMemberOfGroup($id_group, $id_user))
	    {
	        bab_addUserToGroup($id_user, $id_group);
	        $this->writeLog("info", sprintf("The user %s, has been added to group %s", bab_getUserName($id_user), $groupname));
	    }

	}


	/**
	 * Rebuild organizational chart based on newly synchronized user directory.
	 */
	public function rebuildOrgChart($force = false)
	{
	    $registry = authldap_registry();

	    $registry->changeDirectory('orgchart');
	    $rebuildOrgChart = $registry->getValue('rebuild_orgchart', 0);
	    if (!$force && !$rebuildOrgChart) {
			return;
	    }


	    $baseGroup = $registry->getValue('rebuild_orgchart_basegroup', BAB_REGISTERED_GROUP);
	    $entityFieldName = $registry->getValue('entity_field', '');
	    $parentEntityFieldName = $registry->getValue('parent_entity_field', '');
	    $roleFieldName = $registry->getValue('role_field', '');
	    $orgChartName = $registry->getValue('rebuild_orgchart_name', null);

	    $entityFieldId = null;
	    $parentEntityFieldId = null;
	    $roleFieldId = null;

	    $fields = bab_getDirEntry(BAB_REGISTERED_GROUP, BAB_DIR_ENTRY_ID_GROUP);

	    foreach ($fields as $fieldId => $field) {
	        if ($field['name'] == $entityFieldName) {
	            $entityFieldId = $fieldId;
	        }
	    	if ($field['name'] == $parentEntityFieldName) {
	            $parentEntityFieldId = $fieldId;
	        }
  	    	if ($field['name'] == $roleFieldName) {
	            $roleFieldId = $fieldId;
	        }
	    }

	    $users = bab_getGroupsMembers(BAB_REGISTERED_GROUP, true, true);


	    $groups = array();
	    $parentGroups = array();

	    foreach ($users as $user) {
	        $entry = bab_getDirEntry($user['id']);

	        $groupName = $entry[$entityFieldId]['value'];
	        $parentGroupName = $entry[$parentEntityFieldId]['value'];

	        $parentGroups[$parentGroupName] = null;

	        if (!isset($groups[$parentGroupName . '/' . $groupName])) {
    	        $group = array(
    	        	'name' => $groupName,
    	            'parent' => $parentGroupName,
    	            'users' => array(
    	                $user['id'] => $user['id']
                    )
    	        );
    	        $groups[$parentGroupName . '/' . $groupName] = $group;
	        } else {
	            if ($groups[$parentGroupName . '/' . $groupName]['parent'] != $parentGroupName) {
	                $errors[] = 'Group ' . $groupName . ' has more than one parent: "' .  $groups[$parentGroupName . '/' . $groupName]['parent'] . '" and "' . $parentGroupName . '"';
	            }
	            $groups[$parentGroupName . '/' . $groupName]['users'][$user['id']] = $user['id'];
	        }
	    }

	    foreach ($parentGroups as $parentGroupName => &$group) {
	    	if (!empty($parentGroupName)) {
		    	$groupId = bab_createGroup($parentGroupName, '', 0, $baseGroup);
		    	if ($groupId) {
		    		$group = $groupId;
		    	}
	    	}
	    }
	    foreach ($groups as &$group) {
	    	if (isset($parentGroups[$group['parent']]) && !empty($group['name'])) {
	    		$groupId = bab_createGroup($group['name'], '', 0, $parentGroups[$group['parent']]);
	    		if ($groupId) {
		    		foreach ($group['users'] as $userId) {
						bab_addUserToGroup($userId, $groupId);
		    		}
	    		}
	    	}
	    }


	    $this->createOrgChartFromGroup($orgChartName, $baseGroup);

	}



	/**
	 *
	 * @param string $orgChartName
	 * @param int $baseGroup
	 */
	public function createOrgChartFromGroup($orgChartName = null, $baseGroup = null)
	{
		$registry = authldap_registry();

		$registry->changeDirectory('orgchart');

		if (!isset($baseGroup)) {
			$baseGroup = $registry->getValue('rebuild_orgchart_basegroup', BAB_REGISTERED_GROUP);
		}
		if (!isset($orgChartName)) {
			$orgChartName = $registry->getValue('rebuild_orgchart_name', null);
		}

		$roleFieldName = $registry->getValue('role_field', '');
		$roleFieldId = null;

		$fields = bab_getDirEntry(BAB_REGISTERED_GROUP, BAB_DIR_ENTRY_ID_GROUP);

		foreach ($fields as $fieldId => $field) {
			if ($field['name'] == $roleFieldName) {
				$roleFieldId = $fieldId;
			}
		}

		require_once $GLOBALS['babInstallPath'] . 'utilit/ocapi.php';


		if (!isset($orgChartName)) {
			$this->writeLog('error', 'No organizational chart name defined in configuration.');
			return false;
		}
		$orgChart = new bab_OrgChartUtil(-1);




		if ($orgChart->orgChartExist($orgChartName, 0)) {
			$orgChartId = bab_OCGetOrgChartByName($orgChartName);
			$this->writeLog('info', sprintf('Organizational chart "%s" already exists with id "%d".', $orgChartName, $orgChartId));
			$orgChart = new bab_OrgChartUtil($orgChartId);
			$entities =  bab_OCGetEntities($orgChartId);
			foreach ($entities as $entity) {

				$originalEntity = $orgChart->getEntity($entity['id']);

				$user = bab_OCGetSuperior($entity['id']);

				$originalEntity['superior'] = isset($user['is_user']) ? $user['is_user'] : null;
			}

			bab_OCEmptyOrgChart($orgChartId);

		} else {
			$this->writeLog('info', sprintf('Creating organizational chart "%s"', $orgChartName));
			$orgChartId = $orgChart->create($orgChartName, 'Imported organizational chart', 0, 1);
			require_once $GLOBALS['babInstallPath'] . 'admin/acl.php';
			aclAdd(BAB_OCUPDATE_GROUPS_TBL, BAB_ADMINISTRATOR_GROUP, $orgChartId);
			aclAdd(BAB_OCVIEW_GROUPS_TBL, BAB_REGISTERED_GROUP, $orgChartId);

			$orgChart = new bab_OrgChartUtil($orgChartId);
			$this->writeLog('info', sprintf('Created organizational chart "%s" with id "%d"', $orgChartName, $orgChartId));
		}

		$orgChart->lock();

		$groups = bab_getGroups($baseGroup);
		$groupIds = $groups['id'];


		array_unshift($groupIds, $baseGroup);
		$groupInfo = bab_Groups::get($baseGroup);

		$entityGroups = array(
				$groupInfo['id_parent'] => 0
		);

		foreach ($groupIds as $groupId) {
			$groupInfo = bab_Groups::get($groupId);
			$groupParentId = $groupInfo['id_parent'];
			$parentEntityId = $entityGroups[$groupParentId];
			$entityId = $orgChart->createEntity($parentEntityId, $groupInfo['name'], $groupInfo['description'], '', BAB_OC_TREES_LAST_CHILD, $groupId);
			if ($entityId === false) {
				$this->writeLog('error', sprintf('Error while trying to create entity "%s" under entity id "%d".', $groupInfo['name'], $parentEntityId));
				return false;
			}
			$this->writeLog('info', sprintf('Created entity "%s" under entity id "%d".', $groupInfo['name'], $parentEntityId));
			$entityGroups[$groupId] = $entityId;


			$users = bab_getGroupsMembers($groupId, true, true);

			if (is_array($users)) {
				foreach ($users as $user) {

					$entry = bab_getDirEntry($user['id']);

					$roleName = '';
					if (isset($roleFieldId)) {
						$roleName = $entry[$roleFieldId]['value'];
					}
					if (empty($roleName)) {
						$roleName = 'Membres';
					}

					$role = $orgChart->getRoleByName($entityId, $roleName, BAB_OC_ROLE_CUSTOM);
					if (!$role) {
						$roleId = $orgChart->createRole($entityId, $roleName, '', BAB_OC_ROLE_CUSTOM, 'Y');
					} else {
						$roleId = $role['id'];
					}
					if ($roleId === false) {
						$this->writeLog('error', sprintf('Error while trying to create role in entity "%s".', $groupInfo['name']));
						return false;
					}

					$roleUserId = $orgChart->createRoleUser($roleId, $user['id']);
					if ($roleUserId === false) {
						$this->writeLog('error', sprintf('Error while trying to add user id "%d" in entity "%s".', $user['id'], $groupInfo['name']));
						return false;
					}
				}
			}

		}
		$orgChart->unlock();
	}





	public function sendNotifications()
	{
	    $recipients = $this->server->recipients;

	    if (empty($recipients)) {
	        $this->writeLog('error', authldap_translate('No recipients for notification'));
	        return;
	    }

	    $recipients = preg_split('/[\s,]+/', $recipients);

	    if (empty($recipients)) {
	        $this->writeLog('error', authldap_translate('No recipients for notification'));
	        return;
	    }

	    require_once $GLOBALS['babInstallPath'].'utilit/mailincl.php';

	    $mail = bab_mail();

	    if (false === $mail) {
	        // email disabled
	        $this->writeLog('error', authldap_translate('Emails are disabled'));
	        return;
	    }

	    if (empty($this->messages)) {
	        $this->writeLog('info', authldap_translate('No message to send in notification'));
	        return;
	    }


	    $settings = bab_getInstance('bab_Settings');
	    /*@var $settings bab_Settings */
	    $site = $settings->getSiteSettings();


	    $mail->mailSender($site['adminemail']);
	    $mail->mailSubject(authldap_translate('Ldap Synchronization'));
	    foreach($recipients as $email) {
	        $mail->mailTo($email);
	    }

	    // build body

	    $text = '';
	    $html = '';

	    foreach($this->messages as $list) {

	        $text .= $list[0]->getTitle()."\n";
	        $html .= '<h2>'.bab_toHtml($list[0]->getTitle()).'</h2>';

	        foreach($list as $message) {
	            $text .= $message->getTextMessage();
	            $html .= $message->getHtmlMessage();
	        }
	    }


	    $mail->mailBody($html, 'html');
	    $mail->mailAltBody($text);


	    if ($mail->send()) {
	        $this->writeLog('info', authldap_translate('Notification sent'));
	    } else {
	        $this->writeLog('error', authldap_translate('Notification failed').' '.$mail->ErrorInfo());
	    }
	}
}





/**
 *
 */
function authldap_getRootGroupName($server)
{
    static $name = null;
	if($name === null) {
		$name = array();
	}

    if (!isset($name[$server->id])) {
        if (empty($server->root)) {
            throw new Exception('The root group configuration does not exists');
        }

        $name[$server->id] = bab_getGroupName($server->root, false);

        if (empty($name[$server->id])) {
            throw new Exception('Group not found '.$server->root);
        }
    }

    return $name[$server->id];
}


/**
 * @return int
 */
function authldap_getRootGroupParentId($server)
{
    static $id_parent = null;
	if($id_parent === null) {
		$id_parent = array();
	}

    if (!isset($id_parent[$server->id])) {
        if (empty($server->root)) {
            throw new Exception('The root group configuration does not exists');
        }

        $group = bab_Groups::get($server->root);

        $id_parent[$server->id] = (int) $group['id_parent'];
    }

    return $id_parent[$server->id];
}






/**
 * get ldap group from server
 * return array with keys memberof and member
 *
 * @param string $dn
 * @return array
 */
function authldap_getLdapGroup($link, $dn, $enconding)
{

	$attributes = array(
		'cn',
		'memberof',
		'member'
	);

	$res = ldap_read($link, utf8_encode($dn), "(objectclass=*)", $attributes);

	if (!$res)
	{
		throw new Exception(sprintf("failed to read from DN %s", $dn));
	}

	$entry = ldap_get_entries($link, $res);

	$entry = $entry[0];


	for($i =0; $i < $entry['count']; $i++)
	{
		$field = $entry[$i];
		$values = $entry[$field];
		unset($values['count']);

		foreach($values as $k => $value)
		{
			$entry[$field][$k] = trim(authldap_decode($value, $enconding));
		}
	}

	return $entry;
}


/**
 * get list of groups from root to requested group
 * @throws Exception
 * @return array
 */
function authldap_getLdapGroupAncestors($link, $dn, $list = array(), $encoding)
{
	$group = authldap_getLdapGroup($link, $dn, $encoding);

	array_unshift($list, $group);

	if (empty($group['memberof']))
	{
		return $list;
	}

	$memberOf = $group['memberof'][0];

	return authldap_getLdapGroupAncestors($link, $memberOf, $list, $encoding);
}


/**
 * get the list of ovidentia groups under root group
 * @param	int	$id_user
 * @param	int	$id_group
 *
 * @return array
 */
function authldap_getUserGroupsUnderRoot($id_user, $server)
{
	$current_groups = bab_getUserGroups($id_user);
	$selected_groups = array();

	foreach ($current_groups['id'] as $pos => $id) {
		if ($id == BAB_ADMINISTRATOR_GROUP || $id == BAB_REGISTERED_GROUP) {
			continue;
		}

		$ancestors = bab_Groups::getAncestors($id);
		foreach ($ancestors as $name) {
			if ($name === authldap_getRootGroupName($server)) {
				$selected_groups[$id] = $current_groups['name'][$pos];
			}
		}
	}

	return $selected_groups;
}


function authldap_getLevelGoupId($id_parent, $name)
{
	$level = bab_getGroups($id_parent, false);

	if (!isset($level['name'])) {
	    return 0;
	}

	foreach($level['name'] as $key => $grpname)
	{
		if (mb_strtolower($grpname) == mb_strtolower($name))
		{
			return $level['id'][$key];
		}
	}

	return 0;
}







function authldap_getDirectoryMapping($server)
{
    $dirFields = bab_getDirectoriesFields(array(0));
    $serverSet = authldap_ServerSet();
    
    $mapping = array();
    
    foreach ($dirFields as $dirField){
        $fieldName = $dirField['name'];
        if($serverSet->fieldExist($fieldName) && !empty($server->$fieldName)){
            $mapping[$fieldName] = $server->$fieldName;
        }
    }
    $mapping['login'] = $server->login;
    
    return $mapping;
}



/**
 * return array
 */
function authldap_getAttributes($server)
{
	
	$attributes = array("dn", "modifytimestamp");
	$dirFields = bab_getDirectoriesFields(array(0));
	$serverSet = authldap_ServerSet();
	
    foreach ($dirFields as $dirField){
        $fieldName = $dirField['name'];
        if($serverSet->fieldExist($fieldName) && !empty($server->$fieldName)){
            $attributes[] = $server->$fieldName;
        }
    }

	return $attributes;
}







/**
 *
 * @param string $str
 * @return string
 */
function authldap_decode($str, $encoding)
{
	switch ($encoding)
	{
		case 1: // site setting
		case 'utf-8':

			return bab_getStringAccordingToDataBase($str, 'UTF-8');

			break;

		case 't61':
			$str = ldap_t61_to_8859($str);

		case 0: // site setting
		case 'iso-8859-1':
		default:
			return bab_getStringAccordingToDataBase($str, 'ISO-8859-1');
			break;
	}

}
