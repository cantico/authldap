<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
bab_Widgets()->includePhpClass('Widget_Form');
bab_Widgets()->includePhpClass('Widget_Frame');
require_once $GLOBALS['babInstallPath'].'/utilit/dirincl.php';


class authldap_ServerEditor extends Widget_Form
{

    private $defaultMapping = array(
        'cn',
        'departmentnumber',
        'facsimiletelephonenumber',
        'givenname',
        'homephone',
        'homepostaladdress',
        'jpegphoto',
        'l',
        'mail',
        'mobile',
        'o',
        'postalcode',
        'sn',
        'st',
        'street',
        'telephonenumber',
        'title',
        'uid'
    );
    private $mapping;
    
    private $ldapGenericConfiguration;
    
	public function __construct()
	{
	    $dirFields = bab_getDirectoriesFields(array(0));
		$W = bab_Widgets();
		authldap_IncludeSet();
		
		$set = authldap_ServerSet();
		
		$this->mapping = array();
		foreach ($dirFields as $dirField){
		    $this->mapping[$dirField['name']] = $dirField['description'];
		}
		
		$hasAllFields = true;
		foreach ($this->mapping as $key => $value){
		    if(!$set->fieldExist($key)){
		        $hasAllFields = false;
		    }
		}
		if(!$hasAllFields){
		    bab_getBody()->addError(
		        authldap_translate('The authldap addon fields for the user do not match the Ovidentia directory fields. Therefore, some of the user mapping fields will not be saved. Please update the authldap addon.')
	        );
		}

		try{
		    bab_debug(authldap_getLdapGenericConfiguration());
		    $this->ldapGenericConfiguration = authldap_getLdapGenericConfiguration();
		}
		catch(Exception $e){
		    //Possible exception if ovldap-ini.php is not accessible or if the global password is empty
		    //We do nothing in catch because in this editor, this exception must not be blocking
		    //We wiil just add default values to the synchronization fields
		    $this->ldapGenericConfiguration = array();
		}
		
		parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(2,'em'));
		$this->setName('serverOptions');
		$this->addClass('widget-bordered');
		$this->addClass('BabLoginMenuBackground');
		$this->addClass('widget-centered');
		$this->colon();
		
		$this->setHiddenValue('tg', 'addon/authldap/main');
		$this->addItem($W->Hidden()->setName('serverId'));
		$this->addItems();
        $this->addButtons();
	}
	
	public function setValues($row, $namePathBase = array())
	{
	    
	    if(isset($row['administratorPassword'])){
	        $pwd = array(1 => $row['administratorPassword'], 2 => $row['administratorPassword']);
	        $row['administratorPassword'] = $pwd;
	    }
	    if(isset($row['id'])){
	        $row['serverId'] = $row['id'];
	    }

	    foreach ($row as $namePath => $value) {
	        $tmp = $namePathBase;
	        $tmp[] = $namePath;
	        if (is_array($value)) {
	            $this->setValues($value, $tmp);
	        } else {
	            $this->setValue($tmp, $value);
	        }
	    }
	    $allMapping = array_merge($this->defaultMapping, array_flip($this->mapping));
	    foreach ($allMapping as $mapping){
	        if(isset($row[$mapping]) && !empty($row[$mapping])){
	            $tmp = $namePathBase;
	            $tmp[] = $mapping.'Value';
	            $select = $namePathBase;
	            $select[] = $mapping;
	            if (is_array($row[$mapping])) {
	                $this->setValues($row[$mapping], $tmp);
	            }
	            else {
	                if(!in_array($row[$mapping], $allMapping)){
	                    $this->setValue($tmp, $row[$mapping]);
	                    $this->setValue($select, 'other');
	                }
	            }
	        }
	    }
	    return $this;
	}

	protected function addButtons()
	{
	    $W = bab_Widgets();
	
	    $button = $W->FlowItems(
	        $W->SubmitButton()
	        ->validate()
	        ->setAction(authldap_Controller()->Server()->saveServer())
	        ->setName('save')->setLabel(authldap_translate('Save'))
        )->setSpacing(1, 'em');
	
        $this->addItem($button);
	}
	
	protected function addItems()
	{
	    $W = bab_Widgets();
	    
	    $box = $W->VBoxItems();
	    $adBox = $W->VBoxItems();
	    $ldapBox = $W->VBoxItems();
	    
	    $adBox->addItems(
	       $this->domainName()    
	    );
	    
	    $ldapBox->addItems(
            $this->passwordEncodeType(),
	        $this->administratorDN(),
	        $this->administratorPassword(),
	        $this->userDN()
	    );
	    $ldapMapping = $W->VBoxItems();
	    
	    $selectLdap = $W->Select()->setName('login');
	    $selectLdap->addOption('', '');
	     
	    foreach ($this->defaultMapping as $map){
	        $selectLdap->addOption($map, $map);
	    }
	    $selectLdap->addOption('other', authldap_translate('Other'));
        $input = $W->LineEdit()->setName('loginValue');
        $selectLdap->setAssociatedDisplayable($input, array('other'));
	    $ldapMapping->addItem(
	        $W->FlowItems(
                $W->LabelledWidget(
                    authldap_translate('Login'),
                    $selectLdap->setMandatory(true, sprintf(authldap_translate('%s is mandatory'), authldap_translate('Login')))
                ),
                $input
            )
        );
	     
	    $select = $W->Select()->setValue('ldap')->setName('serverType');
	    $select->addOption(authldap_ServerSet::SERVER_LDAP, authldap_translate('LDAP'));
	    $select->addOption(authldap_ServerSet::SERVER_AD, authldap_translate('Active Directory'));
	    
	    $select->setAssociatedDisplayable($ldapBox, array(authldap_ServerSet::SERVER_LDAP));
	    $select->setAssociatedDisplayable($ldapMapping, array(authldap_ServerSet::SERVER_LDAP));
	    $select->setAssociatedDisplayable($adBox, array(authldap_ServerSet::SERVER_AD));
	    
	    $selectLabel = $W->LabelledWidget(
	        authldap_translate('Server type'), 
	        $select
	    );
	    
	    $mappingSection = $W->Section(
	        authldap_translate('User mapping'),
	        $W->VBoxItems(
	            $ldapMapping,
	            $this->userMapping()
	        )
	    )->setFoldable(true);
	    
	    // SERVER CONFIGURATION
	    
	    $box->addItems(
	        $W->Section(
	            authldap_translate('Server configuration'),
	            $W->VBoxItems(
	                $this->serverName(),
	                $selectLabel,
	                $this->serverAddress(),
	                $this->serverPort(),
	                $this->searchBase(),
	                $ldapBox,
	                $adBox,
	                $this->filter(),
	                $this->serverEncodeType()
	            )
	        )->setFoldable(true),
	        $mappingSection
	    );
		
	    // SYNCHRONIZATION CONFIGURATION
	    	    
	    $timerActive = $this->timer_active();
		$timerHour = $this->timer_hour();
		$timerActive->setAssociatedDisplayable($timerHour, array(true));
		
		$syncActive = $this->sync();
		
		$syncBox = $W->VBoxItems(
		    $W->Section(
		        authldap_translate('Configuration'),
		        $W->VBoxItems(
    		        $this->sync_filter(),
    		        $this->last_synchronization(),
    		        $this->sync_login(),
    		        $this->sync_password()
    	        )->setVerticalSpacing(0.5, 'em')
		    ),
	        $W->Section(
	            authldap_translate('General'),
	            $W->VBoxItems(
	                $timerActive,
	                $timerHour,
	                $this->logfile(),
	                $this->is_confirmed(),
	                $this->disableusers(),
	                $this->neverdisableadmin(),
	                $this->neverdisableauth(),
	                $this->defaultemail(),
	                $this->password()
                )->setVerticalSpacing(0.5, 'em')
            ),
            $W->Section(
                authldap_translate('Users'),
                $W->VBoxItems(
                    $this->userfield(),
                    $this->root(),
                    $this->create_groups(),
                    $this->remove_from_group(),
                    $this->remove_from_groups_on_disable(),
                    $this->group_path_method()
                )->setVerticalSpacing(0.5, 'em')
            )->setFoldable(false, false),
            $W->Section(
                authldap_translate('Groups from a field'),
                $W->VBoxItems(
                    $this->groupsattribute(),
                    $this->grouproot()
                )->setVerticalSpacing(0.5, 'em')
            )->setFoldable(false, false),
            $W->Section(
                authldap_translate('Notifications'),
                $W->VBoxItems(
                    $this->recipients()
                )->setVerticalSpacing(0.5, 'em')
            )->setFoldable(false, false)
		);
		
		$syncActive->setAssociatedDisplayable($syncBox, array(true));
	    
		$box->addItem(
			$W->Section(
				authldap_translate('Synchronization'),
				$W->VBoxItems(
				    $syncActive,
                    $syncBox
				)->setVerticalSpacing(.5, 'em')
			)->setFoldable(true, false)
		);
	    
	    $this->addItem($box);
	}
	
	protected function sync_filter()
	{
	    $W = bab_Widgets();
	    return $W->LabelledWidget(
            authldap_translate('Synchronisation filter'),
            $W->LineEdit()->setValue('(|(objectclass=person))'),
            __FUNCTION__
        );
	}
	
	
	protected function last_synchronization()
	{
		$W = bab_Widgets();
		
		return $W->LabelledWidget(
			authldap_translate('Last synchronisation'),
			$W->LineEdit()->setDisplayMode(),
			__FUNCTION__
		);
	}
	
	protected function sync_login()
	{
	    $W = bab_Widgets();
	
	    return $W->LabelledWidget(
	        authldap_translate('Login'),
	        $W->LineEdit()->setMandatory(true, sprintf(authldap_translate('%s is mandatory'), authldap_translate('Synchronization login'))),
	        __FUNCTION__
        );
	}
	
	protected function sync_password()
	{
	    $W = bab_Widgets();
	
	    return $W->LabelledWidget(
	        authldap_translate('Password'),
	        $W->LineEdit()->setMandatory(true, sprintf(authldap_translate('%s is mandatory'), authldap_translate('Synchronization password'))),
	        __FUNCTION__
        );
	}
	
	
	protected function sync()
	{
		$W = bab_Widgets();
		
		return $W->LabelledWidget(
			authldap_translate('Enable'),
			$W->CheckBox(),
			__FUNCTION__
		);
	}
	
	
	protected function recipients()
	{
		$W = bab_Widgets();
		
		return $W->LabelledWidget(
			authldap_translate('Recipients, coma separated emails list'),
			$W->LineEdit(),
			__FUNCTION__
		);
	}
	
	
	protected function group_path_method()
	{
		$W = bab_Widgets();
		
		$options = array(
			'dn' => authldap_translate('Use the full DN path'),
			'memberof' => authldap_translate('Use the memberof attribute')
		);
		
		return $W->LabelledWidget(
			authldap_translate('Method used to get the group path'),
			$W->Select()->setOptions($options),
			__FUNCTION__
		);
	}
	
	
	protected function remove_from_groups_on_disable()
	{
		$W = bab_Widgets();
		
		return $W->LabelledWidget(
			authldap_translate('If a user does not exists in LDAP, remove user from groups under the configured root group'),
			$W->CheckBox(),
			__FUNCTION__
		);
	}
	
	
	protected function remove_from_group()
	{
		$W = bab_Widgets();
		
		return $W->LabelledWidget(
			authldap_translate('Remove users from group if group not found in ldap group tree'),
			$W->CheckBox(),
			__FUNCTION__
		);
	}
	
	
	protected function create_groups()
	{
		$W = bab_Widgets();
		
		return $W->LabelledWidget(
			authldap_translate('Create groups in Ovidentia if not exists'),
			$W->CheckBox(),
			__FUNCTION__
		);
	}
	
	
	protected function userfield()
	{
		$W = bab_Widgets();
		
		return $W->LabelledWidget(
			authldap_translate('User entry field name with the group list'),
			$W->LineEdit()->setValue('memberOf'),
			__FUNCTION__
		);
	}
	
	
	protected function root()
	{
		$W = bab_Widgets();
		
		return $W->LabelledWidget(
			authldap_translate('Common group in ovidentia and LDAP used as a root node for synchronization'),
			$W->GroupPicker()->setMandatory(true)->setValue(1),
			__FUNCTION__
		);
	}
	
	
	protected function grouproot()
	{
		$W = bab_Widgets();
		
		return $W->LabelledWidget(
			authldap_translate('Create groups under'),
			$W->GroupPicker()->setMandatory(true)->setValue(1),
			__FUNCTION__
		);
	}
	
	
	protected function groupsattribute()
	{
		$W = bab_Widgets();
		
		return $W->LabelledWidget(
			authldap_translate('User entry field name with the groups names'),
			$W->LineEdit(),
			__FUNCTION__
		);
	}
	
	
	protected function timer_active()
	{
		$W = bab_Widgets();
		
		return $W->LabelledWidget(
			authldap_translate('Call synchronization with a daily timer'),
			$W->CheckBox(),
			__FUNCTION__
		);
	}
	
	
	protected function timer_hour()
	{
		$W = bab_Widgets();
		
		$options = array();
		for($h=0; $h<24; $h++)
		{
			$options[$h] = sprintf('%02d:00', $h);
		}
		
		return $W->LabelledWidget(
			authldap_translate('Process the daily synchronization at'),
			$W->Select()->setOptions($options)->setValue(23),
			__FUNCTION__
		);
	}
	
	
	protected function logfile()
	{
		$W = bab_Widgets();
		
		return $W->LabelledWidget(
			authldap_translate('Log file full path'),
			$W->LineEdit()->setMandatory(true, sprintf(authldap_translate('%s is mandatory'), authldap_translate('Log file full path')))->setValue('/var/log/ovidentia/%s-ldap.log'),
			__FUNCTION__,
			authldap_translate('%s will be replaced by the server name')
		);
	}
	
	
	protected function is_confirmed()
	{
		$W = bab_Widgets();
		
		$options = array(
			'1' => authldap_translate('Enabled'),
			'0' => authldap_translate('Disabled')
		);
		
		return $W->LabelledWidget(
			authldap_translate('Status for new accounts'),
			$W->Select()->setOptions($options),
			__FUNCTION__
		);
	}
	
	
	protected function disableusers()
	{
		$W = bab_Widgets();
		
		return $W->LabelledWidget(
			authldap_translate('Disable ovidentia users not present in LDAP server'),
			$W->CheckBox(),
			__FUNCTION__,
		    authldap_translate('Warning : if multiple LDAP/AD authentication servers are configured, every user who is not present in this server will be disabled when syncing')
		);
	}
	
	
	protected function neverdisableadmin()
	{
		$W = bab_Widgets();
		
		return $W->LabelledWidget(
			authldap_translate('Never disable ovidentia administrators'),
			$W->CheckBox(),
			__FUNCTION__
		);
	}
	
	
	protected function neverdisableauth()
	{
		$W = bab_Widgets();
		
		return $W->LabelledWidget(
			authldap_translate('Never disable users without LDAP authentication'),
			$W->CheckBox(),
			__FUNCTION__
		);
	}
	
	
	protected function defaultemail()
	{
		$W = bab_Widgets();
		
		return $W->LabelledWidget(
			authldap_translate('Default email if no mail in ldap entry'),
			$W->EmailLineEdit()->setMandatory(true)->setValue('register@cantico.fr'),
			__FUNCTION__
		);
	}
	
	
	protected function password()
	{
		$W = bab_Widgets();
		
		return $W->LabelledWidget(
			authldap_translate('Default password (not the ldap one)'),
			$W->LineEdit()->setMandatory(true)->setValue('ovidentia'),
			__FUNCTION__
		);
	}
	
	
	protected function serverName()
	{
	    $W = bab_Widgets();
	    return $W->LabelledWidget(
	        authldap_translate('Server name'),
	        $W->LineEdit()->setMandatory(true, authldap_translate('The server name is mandatory')),
	        'name'
        );
	}
	
	protected function serverAddress()
	{
	    $W = bab_Widgets();
	    return $W->LabelledWidget(
           authldap_translate('Server'), 
           $W->LineEdit()->setMandatory(true, authldap_translate('The server address is mandatory')),
           __FUNCTION__
        );
	}
	
	protected function serverPort()
	{
	    $W = bab_Widgets();
	    return $W->LabelledWidget(
	        authldap_translate('Port'),
	        $W->LineEdit(),
	        __FUNCTION__
        );
	}
	
	protected function domainName()
	{
	    $W = bab_Widgets();
	    return $W->LabelledWidget(
            authldap_translate('Domain name'),
            $W->LineEdit()->setMandatory(true, authldap_translate('The domain name is mandatory')),
            __FUNCTION__
        );
	}
	
	protected function searchBase()
	{
	    $W = bab_Widgets();
	    return $W->LabelledWidget(
            authldap_translate('LDAP search base'),
            $W->LineEdit(),
            __FUNCTION__
        );
	}
	
	protected function filter()
	{
	    $W = bab_Widgets();
	    return $W->LabelledWidget(
            authldap_translate('Filter'),
            $W->LineEdit(),
            __FUNCTION__
        );
	}
	
	protected function serverEncodeType()
	{
	    $W = bab_Widgets();
	    $select = $W->Select()
           ->addOption('0', 'ISO-8859-1')
	       ->addOption('1', 'UTF-8');
	    
	    return $W->LabelledWidget(
            authldap_translate('Server characted encoding'),
	        $select,
           __FUNCTION__
        );
	}
	
	protected function passwordEncodeType()
	{
	    $W = bab_Widgets();
	    $select = $W->Select()
    	    ->addOption('', '')
    	    ->addOption('plain', authldap_translate('Plain'))
    	    ->addOption('crypt', authldap_translate('Based on DES crypt'))
    	    ->addOption('sha', authldap_translate('SHA-1'))
    	    ->addOption('md5-base64', authldap_translate('MD5 encoded in base64'))
    	    ->addOption('ssha', authldap_translate('Salted SHA-1'))
    	    ->addOption('smd5', authldap_translate('Salted MD5'));
	     
	    return $W->LabelledWidget(
            authldap_translate('Password characted encoding'),
            $select,
            __FUNCTION__
        );
	}
	
	protected function administratorDN()
	{
	    $W = bab_Widgets();
	    return $W->LabelledWidget(
            authldap_translate('Administrator DN'),
            $W->LineEdit(),
            __FUNCTION__
        );
	}
	
	protected function administratorPassword()
	{
	    $W = bab_Widgets();
	    
	    $inputWidget1 = $W->LineEdit('adminPwdInput1')->obfuscate()->setName(array(__FUNCTION__, '1'));
	    $label1 = $W->LabelledWidget(authldap_translate('Administrator password'), $inputWidget1);
	    $inputWidget2 = $W->LineEdit('adminPwdInput2')->obfuscate()->setName(array(__FUNCTION__, '2'));
	    $label2 = $W->LabelledWidget(authldap_translate('Confirm administrator password'), $inputWidget2);
	    
        $layout = $W->FlowLayout('adminPwdInputs')
	        ->setVerticalSpacing(0.2, 'em')
	        ->setHorizontalSpacing(2, 'em')
            ->setMetadata('differentPwdMessage', authldap_translate('Passwords must be identicals'));
        
	    $layout->addItem($label1);
	    $layout->addItem($label2);
	    
	    return $layout;
	}
	
	protected function userDN()
	{
	    $W = bab_Widgets();
	    return $W->LabelledWidget(
            authldap_translate('User DN'),
            $W->LineEdit(),
            __FUNCTION__
        );
	}
	
	protected function userMapping()
	{
	    $W = bab_Widgets();
 
	    $inputsBox = $W->VBoxItems();
	    
	    $mandatory = array('sn', 'givenname', 'email');
	    
	    foreach ($this->mapping as $name => $description){
	        $select = $W->Select()->setName($name);
	        $select->addOption('', '');
	        foreach ($this->defaultMapping as $map){
	            $select->addOption($map, $map);
	        }
	        if(in_array($name, $mandatory)){
	            $select->setMandatory(true, sprintf(authldap_translate('%s is mandatory'), $description));
	        }
	        $select->addOption('other', authldap_translate('Other'));
	        $input = $W->LineEdit()
	           ->setName($name.'Value')
	           ->setMandatory(true, sprintf(authldap_translate('%s is mandatory'), $description));
	        $select->setAssociatedDisplayable($input, array('other'));
	        $inputsBox->addItem(
	           $W->FlowItems(
	                   $W->LabelledWidget($description, $select),
	                   $input
	           )    
	        );
	    }
	    
	    return $inputsBox;
	}
}

class authldap_ServerList extends Widget_Frame
{
    
    private $servers;
    
    public function __construct($servers)
    {
        $W = bab_Widgets();
        authldap_IncludeSet();
    
        parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(2,'em'));
        
        $this->servers = $servers;
        
        $this->addItems();
    }
    
    protected function addItems()
    {
        $W = bab_Widgets();
        
        $serverBox = $W->ListItems();
        
        foreach ($this->servers as $server){
            $serverBox->addItem(
                $this->serverInfo($server)
            );
        }
        
        $this->addItem(
            $W->Section(
                authldap_translate('Servers list'),
                $serverBox
            )
        );
    }
    
    protected function serverInfo(authldap_Server $server)
    {
        $W = bab_Widgets();
        
        $serverBox = $W->HBoxItems();
        $serverBox->addItems(
            $W->Link($server->name, authldap_Controller()->Server()->edit($server->id))
        );
        
        return $serverBox;
    }
}